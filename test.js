(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.jsPanel = void 0;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var jsPanel = {
  version: "4.6.0",
  date: "2019-03-27 10:00",
  ajaxAlwaysCallbacks: [],
  autopositionSpacing: 4,
  closeOnEscape: void document.addEventListener("keydown", function (e) {
    "Escape" !== e.key && "Escape" !== e.code && "Esc" !== e.key || jsPanel.getPanels(function () {
      return this.classList.contains("jsPanel");
    }).some(function (e) {
      return !!e.options.closeOnEscape && (jsPanel.close(e), !0);
    });
  }, !1),
  defaults: {
    boxShadow: 3,
    container: "window",
    contentSize: {
      width: "400px",
      height: "200px"
    },
    dragit: {
      cursor: "move",
      handles: ".jsPanel-headerlogo, .jsPanel-titlebar, .jsPanel-ftr",
      opacity: .8,
      disableOnMaximized: !0
    },
    header: !0,
    headerTitle: "jsPanel",
    headerControls: {
      size: "md"
    },
    iconfont: !1,
    maximizedMargin: 0,
    minimizeTo: "default",
    onparentresize: !1,
    paneltype: "standard",
    position: {
      my: "center",
      at: "center",
      of: "window",
      offsetX: "0px",
      offsetY: "0px"
    },
    resizeit: {
      handles: "n, e, s, w, ne, se, sw, nw",
      minWidth: 128,
      minHeight: 128
    },
    theme: "default"
  },
  defaultSnapConfig: {
    sensitivity: 70,
    trigger: "panel"
  },
  extensions: {},
  globalCallbacks: !1,
  icons: {
    close: '<svg class="jsPanel-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28"><path fill="currentColor" d="M15.8,14l9.9-9.9c0.5-0.5,0.5-1.3,0-1.8c-0.5-0.5-1.3-0.5-1.8,0L14,12.3L4.1,2.4c-0.5-0.5-1.3-0.5-1.8,0c-0.5,0.5-0.5,1.3,0,1.8l9.9,9.9l-9.9,9.9c-0.5,0.5-0.5,1.3,0,1.8C2.6,25.9,2.9,26,3.3,26s0.7-0.1,0.9-0.3l9.8-9.9l9.9,9.9c0.3,0.3,0.5,0.3,0.9,0.3s0.6-0.1,0.9-0.3c0.5-0.5,0.5-1.3,0-1.8L15.8,14z"/></svg>',
    maximize: '<svg class="jsPanel-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28"><path fill="currentColor" d="M25.3,1.9H2.7c-0.6,0-1,0.4-1,1v22.3c0,0.5,0.4,1,1,1h22.5c0.5,0,1-0.5,1-1V2.9C26.3,2.3,25.9,1.9,25.3,1.9z M3.7,24.1v-18h20.5v18C24.3,24.1,3.7,24.1,3.7,24.1z"/></svg>',
    normalize: '<svg class="jsPanel-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path fill="currentColor" d="M18.5,1.2H5.1c-0.3,0-0.5,0.3-0.5,0.5v3.1c0,0.1,0,0.1,0,0.2h-3C1.2,5,0.9,5.3,0.9,5.7v12.4c0,0.4,0.3,0.7,0.7,0.7h12.6c0.4,0,0.7-0.3,0.7-0.7v-2.6c0,0,0.1,0,0.1,0h3.5c0.3,0,0.5-0.3,0.5-0.5V1.7C19.1,1.4,18.8,1.2,18.5,1.2z M2.3,17.4V8.2c0,0,0.1,0,0.1,0h11c0,0,0.1,0,0.1,0v9.3H2.3z M18,14.4h-3c0,0-0.1,0-0.1,0V5.7c0-0.4-0.3-0.7-0.7-0.7H5.6c0-0.1,0-0.1,0-0.2V2.2H18C18,2.2,18,14.4,18,14.4z"/></svg>',
    minimize: '<svg class="jsPanel-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 20"><path fill="currentColor" d="M20.4,18.6H1.8c-0.7,0-1.3-0.5-1.3-1.1s0.6-1.1,1.3-1.1h18.5c0.7,0,1.3,0.5,1.3,1.1S21,18.6,20.4,18.6z"/></svg>',
    smallifyrev: '<svg class="jsPanel-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 18"><path fill="currentColor" d="M10,15.1L10,15.1c-0.3,0-0.5-0.1-0.6-0.3L1,6.4C0.6,6,0.6,5.5,1,5.1c0.4-0.4,0.9-0.4,1.3,0L10,13l7.8-7.7c0.4-0.4,0.9-0.4,1.3,0c0.3,0.4,0.4,0.9,0,1.3l-8.4,8.4C10.5,15,10.2,15.1,10,15.1z"/></svg>',
    smallify: '<svg class="jsPanel-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 18"><path fill="currentColor" d="M19,13.5l-8.4-8.3c-0.4-0.4-0.9-0.4-1.3,0L1,13.6c-0.4,0.4-0.4,0.9,0,1.3c0.2,0.2,0.4,0.3,0.6,0.3c0.2,0,0.5-0.1,0.6-0.3L10,7l7.8,7.7c0.4,0.4,0.9,0.4,1.3,0C19.4,14.4,19.4,13.8,19,13.5z"/></svg>'
  },
  idCounter: 0,
  isIE: function () {
    return navigator.appVersion.match(/Trident/);
  }(),
  pointerdown: "onpointerdown" in window ? ["pointerdown"] : "ontouchend" in window ? ["touchstart", "mousedown"] : ["mousedown"],
  pointermove: "onpointermove" in window ? ["pointermove"] : "ontouchend" in window ? ["touchmove", "mousemove"] : ["mousemove"],
  pointerup: "onpointerup" in window ? ["pointerup"] : "ontouchend" in window ? ["touchend", "mouseup"] : ["mouseup"],
  polyfills: (void [Element.prototype, Document.prototype, DocumentFragment.prototype].forEach(function (e) {
    e.append = e.append || function () {
      var e = Array.prototype.slice.call(arguments),
          t = document.createDocumentFragment();
      e.forEach(function (e) {
        var n = e instanceof Node;
        t.appendChild(n ? e : document.createTextNode(String(e)));
      }), this.appendChild(t);
    };
  }), window.Element && !Element.prototype.closest && (Element.prototype.closest = function (e) {
    var t,
        n = (this.document || this.ownerDocument).querySelectorAll(e),
        o = this;

    do {
      for (t = n.length; --t >= 0 && n.item(t) !== o;) {
        ;
      }
    } while (t < 0 && (o = o.parentElement));

    return o;
  }), window.NodeList && !NodeList.prototype.forEach && (NodeList.prototype.forEach = function (e, t) {
    t = t || window;

    for (var n = 0; n < this.length; n++) {
      e.call(t, this[n], n, this);
    }
  }), Object.assign || Object.defineProperty(Object, "assign", {
    enumerable: !1,
    configurable: !0,
    writable: !0,
    value: function value(e) {
      if (null == e) throw new TypeError("Cannot convert first argument to object");
      var t = Object(e);

      for (var _e = 1; _e < arguments.length; _e++) {
        var n = arguments[_e];
        if (null == n) continue;
        n = Object(n);
        var o = Object.keys(Object(n));

        for (var _e2 = 0, a = o.length; _e2 < a; _e2++) {
          var _a = o[_e2],
              i = Object.getOwnPropertyDescriptor(n, _a);
          void 0 !== i && i.enumerable && (t[_a] = n[_a]);
        }
      }

      return t;
    }
  }), function () {
    if ("function" == typeof window.CustomEvent) return !1;

    function e(e, t) {
      t = t || {
        bubbles: !1,
        cancelable: !1,
        detail: void 0
      };
      var n = document.createEvent("CustomEvent");
      return n.initCustomEvent(e, t.bubbles, t.cancelable, t.detail), n;
    }

    e.prototype = window.Event.prototype, window.CustomEvent = e;
  }(), String.prototype.endsWith || (String.prototype.endsWith = function (e, t) {
    return t < this.length ? t |= 0 : t = this.length, this.substr(t - e.length, e.length) === e;
  }), String.prototype.startsWith || (String.prototype.startsWith = function (e, t) {
    return this.substr(t || 0, e.length) === e;
  }), void (String.prototype.includes || (String.prototype.includes = function (e, t) {
    return "number" != typeof t && (t = 0), !(t + e.length > this.length) && -1 !== this.indexOf(e, t);
  }))),
  themes: ["default", "primary", "secondary", "info", "success", "warning", "danger", "light", "dark"],
  ziBase: 100,
  colorFilledLight: .81,
  colorFilledDark: .08,
  colorFilled: 0,
  colorBrightnessThreshold: .55,
  colorNames: {
    aliceblue: "f0f8ff",
    antiquewhite: "faebd7",
    aqua: "0ff",
    aquamarine: "7fffd4",
    azure: "f0ffff",
    beige: "f5f5dc",
    bisque: "ffe4c4",
    black: "000",
    blanchedalmond: "ffebcd",
    blue: "00f",
    blueviolet: "8a2be2",
    brown: "a52a2a",
    burlywood: "deb887",
    cadetblue: "5f9ea0",
    chartreuse: "7fff00",
    chocolate: "d2691e",
    coral: "ff7f50",
    cornflowerblue: "6495ed",
    cornsilk: "fff8dc",
    crimson: "dc143c",
    cyan: "0ff",
    darkblue: "00008b",
    darkcyan: "008b8b",
    darkgoldenrod: "b8860b",
    darkgray: "a9a9a9",
    darkgrey: "a9a9a9",
    darkgreen: "006400",
    darkkhaki: "bdb76b",
    darkmagenta: "8b008b",
    darkolivegreen: "556b2f",
    darkorange: "ff8c00",
    darkorchid: "9932cc",
    darkred: "8b0000",
    darksalmon: "e9967a",
    darkseagreen: "8fbc8f",
    darkslateblue: "483d8b",
    darkslategray: "2f4f4f",
    darkslategrey: "2f4f4f",
    darkturquoise: "00ced1",
    darkviolet: "9400d3",
    deeppink: "ff1493",
    deepskyblue: "00bfff",
    dimgray: "696969",
    dimgrey: "696969",
    dodgerblue: "1e90ff",
    firebrick: "b22222",
    floralwhite: "fffaf0",
    forestgreen: "228b22",
    fuchsia: "f0f",
    gainsboro: "dcdcdc",
    ghostwhite: "f8f8ff",
    gold: "ffd700",
    goldenrod: "daa520",
    gray: "808080",
    grey: "808080",
    green: "008000",
    greenyellow: "adff2f",
    honeydew: "f0fff0",
    hotpink: "ff69b4",
    indianred: "cd5c5c",
    indigo: "4b0082",
    ivory: "fffff0",
    khaki: "f0e68c",
    lavender: "e6e6fa",
    lavenderblush: "fff0f5",
    lawngreen: "7cfc00",
    lemonchiffon: "fffacd",
    lightblue: "add8e6",
    lightcoral: "f08080",
    lightcyan: "e0ffff",
    lightgoldenrodyellow: "fafad2",
    lightgray: "d3d3d3",
    lightgrey: "d3d3d3",
    lightgreen: "90ee90",
    lightpink: "ffb6c1",
    lightsalmon: "ffa07a",
    lightseagreen: "20b2aa",
    lightskyblue: "87cefa",
    lightslategray: "789",
    lightslategrey: "789",
    lightsteelblue: "b0c4de",
    lightyellow: "ffffe0",
    lime: "0f0",
    limegreen: "32cd32",
    linen: "faf0e6",
    magenta: "f0f",
    maroon: "800000",
    mediumaquamarine: "66cdaa",
    mediumblue: "0000cd",
    mediumorchid: "ba55d3",
    mediumpurple: "9370d8",
    mediumseagreen: "3cb371",
    mediumslateblue: "7b68ee",
    mediumspringgreen: "00fa9a",
    mediumturquoise: "48d1cc",
    mediumvioletred: "c71585",
    midnightblue: "191970",
    mintcream: "f5fffa",
    mistyrose: "ffe4e1",
    moccasin: "ffe4b5",
    navajowhite: "ffdead",
    navy: "000080",
    oldlace: "fdf5e6",
    olive: "808000",
    olivedrab: "6b8e23",
    orange: "ffa500",
    orangered: "ff4500",
    orchid: "da70d6",
    palegoldenrod: "eee8aa",
    palegreen: "98fb98",
    paleturquoise: "afeeee",
    palevioletred: "d87093",
    papayawhip: "ffefd5",
    peachpuff: "ffdab9",
    peru: "cd853f",
    pink: "ffc0cb",
    plum: "dda0dd",
    powderblue: "b0e0e6",
    purple: "800080",
    rebeccapurple: "639",
    red: "f00",
    rosybrown: "bc8f8f",
    royalblue: "4169e1",
    saddlebrown: "8b4513",
    salmon: "fa8072",
    sandybrown: "f4a460",
    seagreen: "2e8b57",
    seashell: "fff5ee",
    sienna: "a0522d",
    silver: "c0c0c0",
    skyblue: "87ceeb",
    slateblue: "6a5acd",
    slategray: "708090",
    slategrey: "708090",
    snow: "fffafa",
    springgreen: "00ff7f",
    steelblue: "4682b4",
    tan: "d2b48c",
    teal: "008080",
    thistle: "d8bfd8",
    tomato: "ff6347",
    turquoise: "40e0d0",
    violet: "ee82ee",
    wheat: "f5deb3",
    white: "fff",
    whitesmoke: "f5f5f5",
    yellow: "ff0",
    yellowgreen: "9acd32",
    grey50: "fafafa",
    grey100: "f5f5f5",
    grey200: "eee",
    grey300: "e0e0e0",
    grey400: "bdbdbd",
    grey500: "9e9e9e",
    grey600: "757575",
    grey700: "616161",
    grey800: "424242",
    grey900: "212121",
    gray50: "fafafa",
    gray100: "f5f5f5",
    gray200: "eee",
    gray300: "e0e0e0",
    gray400: "bdbdbd",
    gray500: "9e9e9e",
    gray600: "757575",
    gray700: "616161",
    gray800: "424242",
    gray900: "212121",
    bluegrey50: "eceff1",
    bluegrey100: "CFD8DC",
    bluegrey200: "B0BEC5",
    bluegrey300: "90A4AE",
    bluegrey400: "78909C",
    bluegrey500: "607D8B",
    bluegrey600: "546E7A",
    bluegrey700: "455A64",
    bluegrey800: "37474F",
    bluegrey900: "263238",
    bluegray50: "eceff1",
    bluegray100: "CFD8DC",
    bluegray200: "B0BEC5",
    bluegray300: "90A4AE",
    bluegray400: "78909C",
    bluegray500: "607D8B",
    bluegray600: "546E7A",
    bluegray700: "455A64",
    bluegray800: "37474F",
    bluegray900: "263238",
    red50: "FFEBEE",
    red100: "FFCDD2",
    red200: "EF9A9A",
    red300: "E57373",
    red400: "EF5350",
    red500: "F44336",
    red600: "E53935",
    red700: "D32F2F",
    red800: "C62828",
    red900: "B71C1C",
    reda100: "FF8A80",
    reda200: "FF5252",
    reda400: "FF1744",
    reda700: "D50000",
    pink50: "FCE4EC",
    pink100: "F8BBD0",
    pink200: "F48FB1",
    pink300: "F06292",
    pink400: "EC407A",
    pink500: "E91E63",
    pink600: "D81B60",
    pink700: "C2185B",
    pink800: "AD1457",
    pink900: "880E4F",
    pinka100: "FF80AB",
    pinka200: "FF4081",
    pinka400: "F50057",
    pinka700: "C51162",
    purple50: "F3E5F5",
    purple100: "E1BEE7",
    purple200: "CE93D8",
    purple300: "BA68C8",
    purple400: "AB47BC",
    purple500: "9C27B0",
    purple600: "8E24AA",
    purple700: "7B1FA2",
    purple800: "6A1B9A",
    purple900: "4A148C",
    purplea100: "EA80FC",
    purplea200: "E040FB",
    purplea400: "D500F9",
    purplea700: "AA00FF",
    deeppurple50: "EDE7F6",
    deeppurple100: "D1C4E9",
    deeppurple200: "B39DDB",
    deeppurple300: "9575CD",
    deeppurple400: "7E57C2",
    deeppurple500: "673AB7",
    deeppurple600: "5E35B1",
    deeppurple700: "512DA8",
    deeppurple800: "4527A0",
    deeppurple900: "311B92",
    deeppurplea100: "B388FF",
    deeppurplea200: "7C4DFF",
    deeppurplea400: "651FFF",
    deeppurplea700: "6200EA",
    indigo50: "E8EAF6",
    indigo100: "C5CAE9",
    indigo200: "9FA8DA",
    indigo300: "7986CB",
    indigo400: "5C6BC0",
    indigo500: "3F51B5",
    indigo600: "3949AB",
    indigo700: "303F9F",
    indigo800: "283593",
    indigo900: "1A237E",
    indigoa100: "8C9EFF",
    indigoa200: "536DFE",
    indigoa400: "3D5AFE",
    indigoa700: "304FFE",
    blue50: "E3F2FD",
    blue100: "BBDEFB",
    blue200: "90CAF9",
    blue300: "64B5F6",
    blue400: "42A5F5",
    blue500: "2196F3",
    blue600: "1E88E5",
    blue700: "1976D2",
    blue800: "1565C0",
    blue900: "0D47A1",
    bluea100: "82B1FF",
    bluea200: "448AFF",
    bluea400: "2979FF",
    bluea700: "2962FF",
    lightblue50: "E1F5FE",
    lightblue100: "B3E5FC",
    lightblue200: "81D4FA",
    lightblue300: "4FC3F7",
    lightblue400: "29B6F6",
    lightblue500: "03A9F4",
    lightblue600: "039BE5",
    lightblue700: "0288D1",
    lightblue800: "0277BD",
    lightblue900: "01579B",
    lightbluea100: "80D8FF",
    lightbluea200: "40C4FF",
    lightbluea400: "00B0FF",
    lightbluea700: "0091EA",
    cyan50: "E0F7FA",
    cyan100: "B2EBF2",
    cyan200: "80DEEA",
    cyan300: "4DD0E1",
    cyan400: "26C6DA",
    cyan500: "00BCD4",
    cyan600: "00ACC1",
    cyan700: "0097A7",
    cyan800: "00838F",
    cyan900: "006064",
    cyana100: "84FFFF",
    cyana200: "18FFFF",
    cyana400: "00E5FF",
    cyana700: "00B8D4",
    teal50: "E0F2F1",
    teal100: "B2DFDB",
    teal200: "80CBC4",
    teal300: "4DB6AC",
    teal400: "26A69A",
    teal500: "009688",
    teal600: "00897B",
    teal700: "00796B",
    teal800: "00695C",
    teal900: "004D40",
    teala100: "A7FFEB",
    teala200: "64FFDA",
    teala400: "1DE9B6",
    teala700: "00BFA5",
    green50: "E8F5E9",
    green100: "C8E6C9",
    green200: "A5D6A7",
    green300: "81C784",
    green400: "66BB6A",
    green500: "4CAF50",
    green600: "43A047",
    green700: "388E3C",
    green800: "2E7D32",
    green900: "1B5E20",
    greena100: "B9F6CA",
    greena200: "69F0AE",
    greena400: "00E676",
    greena700: "00C853",
    lightgreen50: "F1F8E9",
    lightgreen100: "DCEDC8",
    lightgreen200: "C5E1A5",
    lightgreen300: "AED581",
    lightgreen400: "9CCC65",
    lightgreen500: "8BC34A",
    lightgreen600: "7CB342",
    lightgreen700: "689F38",
    lightgreen800: "558B2F",
    lightgreen900: "33691E",
    lightgreena100: "CCFF90",
    lightgreena200: "B2FF59",
    lightgreena400: "76FF03",
    lightgreena700: "64DD17",
    lime50: "F9FBE7",
    lime100: "F0F4C3",
    lime200: "E6EE9C",
    lime300: "DCE775",
    lime400: "D4E157",
    lime500: "CDDC39",
    lime600: "C0CA33",
    lime700: "AFB42B",
    lime800: "9E9D24",
    lime900: "827717",
    limea100: "F4FF81",
    limea200: "EEFF41",
    limea400: "C6FF00",
    limea700: "AEEA00",
    yellow50: "FFFDE7",
    yellow100: "FFF9C4",
    yellow200: "FFF59D",
    yellow300: "FFF176",
    yellow400: "FFEE58",
    yellow500: "FFEB3B",
    yellow600: "FDD835",
    yellow700: "FBC02D",
    yellow800: "F9A825",
    yellow900: "F57F17",
    yellowa100: "FFFF8D",
    yellowa200: "FFFF00",
    yellowa400: "FFEA00",
    yellowa700: "FFD600",
    amber50: "FFF8E1",
    amber100: "FFECB3",
    amber200: "FFE082",
    amber300: "FFD54F",
    amber400: "FFCA28",
    amber500: "FFC107",
    amber600: "FFB300",
    amber700: "FFA000",
    amber800: "FF8F00",
    amber900: "FF6F00",
    ambera100: "FFE57F",
    ambera200: "FFD740",
    ambera400: "FFC400",
    ambera700: "FFAB00",
    orange50: "FFF3E0",
    orange100: "FFE0B2",
    orange200: "FFCC80",
    orange300: "FFB74D",
    orange400: "FFA726",
    orange500: "FF9800",
    orange600: "FB8C00",
    orange700: "F57C00",
    orange800: "EF6C00",
    orange900: "E65100",
    orangea100: "FFD180",
    orangea200: "FFAB40",
    orangea400: "FF9100",
    orangea700: "FF6D00",
    deeporange50: "FBE9E7",
    deeporange100: "FFCCBC",
    deeporange200: "FFAB91",
    deeporange300: "FF8A65",
    deeporange400: "FF7043",
    deeporange500: "FF5722",
    deeporange600: "F4511E",
    deeporange700: "E64A19",
    deeporange800: "D84315",
    deeporange900: "BF360C",
    deeporangea100: "FF9E80",
    deeporangea200: "FF6E40",
    deeporangea400: "FF3D00",
    deeporangea700: "DD2C00",
    brown50: "EFEBE9",
    brown100: "D7CCC8",
    brown200: "BCAAA4",
    brown300: "A1887F",
    brown400: "8D6E63",
    brown500: "795548",
    brown600: "6D4C41",
    brown700: "5D4037",
    brown800: "4E342E",
    brown900: "3E2723"
  },
  errorReporting: "on",
  color: function color(e) {
    var t,
        n,
        o,
        a,
        i,
        s,
        l,
        r,
        c,
        d = e.toLowerCase(),
        p = {};
    var h = /^rgba?\(([0-9]{1,3}),([0-9]{1,3}),([0-9]{1,3}),?(0|1|0\.[0-9]{1,2}|\.[0-9]{1,2})?\)$/gi,
        f = /^hsla?\(([0-9]{1,3}),([0-9]{1,3}%),([0-9]{1,3}%),?(0|1|0\.[0-9]{1,2}|\.[0-9]{1,2})?\)$/gi,
        m = this.colorNames;
    return m[d] && (d = m[d]), null !== d.match(/^#?([0-9a-f]{3}|[0-9a-f]{6})$/gi) ? ((d = d.replace("#", "")).length % 2 == 1 ? (t = String(d.substr(0, 1)) + d.substr(0, 1), n = String(d.substr(1, 1)) + d.substr(1, 1), o = String(d.substr(2, 1)) + d.substr(2, 1), p.rgb = {
      r: parseInt(t, 16),
      g: parseInt(n, 16),
      b: parseInt(o, 16)
    }, p.hex = "#".concat(t).concat(n).concat(o)) : (p.rgb = {
      r: parseInt(d.substr(0, 2), 16),
      g: parseInt(d.substr(2, 2), 16),
      b: parseInt(d.substr(4, 2), 16)
    }, p.hex = "#".concat(d)), c = this.rgbToHsl(p.rgb.r, p.rgb.g, p.rgb.b), p.hsl = c, p.rgb.css = "rgb(".concat(p.rgb.r, ",").concat(p.rgb.g, ",").concat(p.rgb.b, ")")) : d.match(h) ? (l = h.exec(d), p.rgb = {
      css: d,
      r: l[1],
      g: l[2],
      b: l[3]
    }, p.hex = this.rgbToHex(l[1], l[2], l[3]), c = this.rgbToHsl(l[1], l[2], l[3]), p.hsl = c) : d.match(f) ? (a = (l = f.exec(d))[1] / 360, i = l[2].substr(0, l[2].length - 1) / 100, s = l[3].substr(0, l[3].length - 1) / 100, r = this.hslToRgb(a, i, s), p.rgb = {
      css: "rgb(".concat(r[0], ",").concat(r[1], ",").concat(r[2], ")"),
      r: r[0],
      g: r[1],
      b: r[2]
    }, p.hex = this.rgbToHex(p.rgb.r, p.rgb.g, p.rgb.b), p.hsl = {
      css: "hsl(".concat(l[1], ",").concat(l[2], ",").concat(l[3], ")"),
      h: l[1],
      s: l[2],
      l: l[3]
    }) : (p.hex = "#f5f5f5", p.rgb = {
      css: "rgb(245,245,245)",
      r: 245,
      g: 245,
      b: 245
    }, p.hsl = {
      css: "hsl(0,0%,96%)",
      h: 0,
      s: "0%",
      l: "96%"
    }), p;
  },
  calcColors: function calcColors(e) {
    var t = this.colorBrightnessThreshold,
        n = this.color(e),
        o = this.lighten(e, this.colorFilledLight),
        a = this.darken(e, this.colorFilled),
        i = this.perceivedBrightness(e) <= t ? "#ffffff" : "#000000",
        s = this.perceivedBrightness(o) <= t ? "#ffffff" : "#000000",
        l = this.perceivedBrightness(a) <= t ? "#ffffff" : "#000000",
        r = this.lighten(e, this.colorFilledDark),
        c = this.perceivedBrightness(r) <= t ? "#ffffff" : "#000000";
    return [n.hsl.css, o, a, i, s, l, r, c];
  },
  darken: function darken(e, t) {
    var n = this.color(e).hsl,
        o = parseFloat(n.l),
        a = Math.round(o - o * t) + "%";
    return "hsl(".concat(n.h, ",").concat(n.s, ",").concat(a, ")");
  },
  lighten: function lighten(e, t) {
    var n = this.color(e).hsl,
        o = parseFloat(n.l),
        a = Math.round(o + (100 - o) * t) + "%";
    return "hsl(".concat(n.h, ",").concat(n.s, ",").concat(a, ")");
  },
  hslToRgb: function hslToRgb(e, t, n) {
    var o, a, i;
    if (0 === t) o = a = i = n;else {
      var s = function s(e, t, n) {
        return n < 0 && (n += 1), n > 1 && (n -= 1), n < 1 / 6 ? e + 6 * (t - e) * n : n < .5 ? t : n < 2 / 3 ? e + (t - e) * (2 / 3 - n) * 6 : e;
      },
          l = n < .5 ? n * (1 + t) : n + t - n * t,
          r = 2 * n - l;

      o = s(r, l, e + 1 / 3), a = s(r, l, e), i = s(r, l, e - 1 / 3);
    }
    return [Math.round(255 * o), Math.round(255 * a), Math.round(255 * i)];
  },
  rgbToHsl: function rgbToHsl(e, t, n) {
    e /= 255, t /= 255, n /= 255;
    var o,
        a,
        i = Math.max(e, t, n),
        s = Math.min(e, t, n),
        l = (i + s) / 2;
    if (i === s) o = a = 0;else {
      var r = i - s;

      switch (a = l > .5 ? r / (2 - i - s) : r / (i + s), i) {
        case e:
          o = (t - n) / r + (t < n ? 6 : 0);
          break;

        case t:
          o = (n - e) / r + 2;
          break;

        case n:
          o = (e - t) / r + 4;
      }

      o /= 6;
    }
    return {
      css: "hsl(" + (o = Math.round(360 * o)) + "," + (a = Math.round(100 * a) + "%") + "," + (l = Math.round(100 * l) + "%") + ")",
      h: o,
      s: a,
      l: l
    };
  },
  rgbToHex: function rgbToHex(e, t, n) {
    var o = Number(e).toString(16),
        a = Number(t).toString(16),
        i = Number(n).toString(16);
    return 1 === o.length && (o = "0".concat(o)), 1 === a.length && (a = "0".concat(a)), 1 === i.length && (i = "0".concat(i)), "#".concat(o).concat(a).concat(i);
  },
  perceivedBrightness: function perceivedBrightness(e) {
    var t = this.color(e).rgb;
    return t.r / 255 * .2126 + t.g / 255 * .7152 + t.b / 255 * .0722;
  },
  addScript: function addScript(e) {
    var t = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "text/javascript";
    var n = arguments.length > 2 ? arguments[2] : undefined;

    if (!document.querySelector("script[src=\"".concat(e, "\"]"))) {
      var o = document.createElement("script");
      o.onload = n, o.src = e, o.type = t, document.head.appendChild(o);
    }
  },
  autopositionRemaining: function autopositionRemaining(e) {
    var t;

    if (["left-top-down", "left-top-right", "center-top-down", "right-top-down", "right-top-left", "left-bottom-up", "left-bottom-right", "center-bottom-up", "right-bottom-up", "right-bottom-left"].forEach(function (n) {
      e.classList.contains(n) && (t = n);
    }), t) {
      ("window" === e.options.container ? document.body : e.options.container).querySelectorAll(".".concat(t)).forEach(function (e) {
        e.reposition();
      });
    }
  },
  ajax: function ajax(obj, ajaxConfig) {
    var objIsPanel;
    "object" == _typeof(obj) && obj.classList.contains("jsPanel") ? objIsPanel = !0 : (objIsPanel = !1, "string" == typeof obj && (obj = document.querySelector(obj)));
    var configDefaults = {
      method: "GET",
      async: !0,
      user: "",
      pwd: "",
      done: function done() {
        objIsPanel ? obj.content.innerHTML = this.responseText : obj.innerHTML = this.responseText;
      },
      autoresize: !0,
      autoreposition: !0
    };
    var config;
    if ("string" == typeof ajaxConfig) config = Object.assign({}, configDefaults, {
      url: encodeURI(ajaxConfig),
      evalscripttags: !0
    });else {
      if ("object" != _typeof(ajaxConfig) || !ajaxConfig.url) {
        if (this.errorReporting) try {
          throw new jsPanel.jsPanelError("XMLHttpRequest seems to miss the <code>url</code> parameter!");
        } catch (e) {
          jsPanel.error(e);
        }
        return obj;
      }

      config = Object.assign({}, configDefaults, ajaxConfig), config.url = encodeURI(ajaxConfig.url), !1 === config.async && (config.timeout = 0, config.withCredentials && (config.withCredentials = void 0), config.responseType && (config.responseType = void 0));
    }
    var xhr = new XMLHttpRequest();
    return xhr.onreadystatechange = function () {
      if (4 === xhr.readyState) {
        if (200 === xhr.status) {
          if (config.done.call(xhr, obj), config.evalscripttags) {
            var scripttags = xhr.responseText.match(/<script\b[^>]*>([\s\S]*?)<\/script>/gi);
            scripttags && scripttags.forEach(function (tag) {
              var js = tag.replace(/<script\b[^>]*>/i, "").replace(/<\/script>/i, "").trim();
              eval(js);
            });
          }
        } else config.fail && config.fail.call(xhr, obj);

        if (config.always && config.always.call(xhr, obj), objIsPanel) {
          var e = obj.options.contentSize;

          if ("string" == typeof e && e.match(/auto/i)) {
            var t = e.split(" "),
                n = Object.assign({}, {
              width: t[0],
              height: t[1]
            });
            config.autoresize && obj.resize(n), obj.classList.contains("jsPanel-contextmenu") || config.autoreposition && obj.reposition();
          } else if ("object" == _typeof(e) && ("auto" === e.width || "auto" === e.height)) {
            var _t = Object.assign({}, e);

            config.autoresize && obj.resize(_t), obj.classList.contains("jsPanel-contextmenu") || config.autoreposition && obj.reposition();
          }
        }

        jsPanel.ajaxAlwaysCallbacks.length && jsPanel.ajaxAlwaysCallbacks.forEach(function (e) {
          e.call(obj, obj);
        });
      }
    }, xhr.open(config.method, config.url, config.async, config.user, config.pwd), xhr.timeout = config.timeout || 0, config.withCredentials && (xhr.withCredentials = config.withCredentials), config.responseType && (xhr.responseType = config.responseType), config.beforeSend && config.beforeSend.call(xhr), config.data ? xhr.send(config.data) : xhr.send(null), obj;
  },
  close: function close(e, t) {
    e.closetimer && window.clearTimeout(e.closetimer);
    var n = e.id,
        o = e.parentElement,
        a = new CustomEvent("jspanelbeforeclose", {
      detail: n
    }),
        i = new CustomEvent("jspanelclosed", {
      detail: n
    });
    if (document.dispatchEvent(a), e.options.onbeforeclose && e.options.onbeforeclose.length > 0 && !jsPanel.processCallbacks(e, e.options.onbeforeclose, "some", e.status)) return e;
    e.options.animateOut ? (e.options.animateIn && jsPanel.remClass(e, e.options.animateIn), jsPanel.setClass(e, e.options.animateOut), e.addEventListener("animationend", function () {
      o.removeChild(e), document.getElementById(n) ? t && t.call(e, n, e) : (jsPanel.removeMinimizedReplacement(e), document.dispatchEvent(i), e.options.onclosed && jsPanel.processCallbacks(e, e.options.onclosed, "every"), jsPanel.autopositionRemaining(e), t && t.call(n, n));
    })) : (o.removeChild(e), document.getElementById(n) ? t && t.call(e, n, e) : (jsPanel.removeMinimizedReplacement(e), document.dispatchEvent(i), e.options.onclosed && jsPanel.processCallbacks(e, e.options.onclosed, "every"), jsPanel.autopositionRemaining(e), t && t.call(n, n)));
  },
  createPanelTemplate: function createPanelTemplate() {
    var e = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : !0;
    var t = document.createElement("div");
    return t.className = "jsPanel", e && ["close", "maximize", "normalize", "minimize", "smallify", "smallifyrev"].forEach(function (e) {
      t.setAttribute("data-btn".concat(e), "enabled");
    }), t.innerHTML = "<div class=\"jsPanel-hdr\">\n                                <div class=\"jsPanel-headerbar\">\n                                    <div class=\"jsPanel-headerlogo\"></div>\n                                    <div class=\"jsPanel-titlebar\">\n                                        <span class=\"jsPanel-title\"></span>\n                                    </div>\n                                    <div class=\"jsPanel-controlbar\">\n                                        <div class=\"jsPanel-btn jsPanel-btn-smallify\">".concat(this.icons.smallify, "</div>\n                                        <div class=\"jsPanel-btn jsPanel-btn-smallifyrev\">").concat(this.icons.smallifyrev, "</div>\n                                        <div class=\"jsPanel-btn jsPanel-btn-minimize\">").concat(this.icons.minimize, "</div>\n                                        <div class=\"jsPanel-btn jsPanel-btn-normalize\">").concat(this.icons.normalize, "</div>\n                                        <div class=\"jsPanel-btn jsPanel-btn-maximize\">").concat(this.icons.maximize, "</div>\n                                        <div class=\"jsPanel-btn jsPanel-btn-close\">").concat(this.icons.close, "</div>\n                                    </div>\n                                </div>\n                                <div class=\"jsPanel-hdr-toolbar\"></div>\n                            </div>\n                            <div class=\"jsPanel-content\"></div>\n                            <div class=\"jsPanel-minimized-box\"></div>\n                            <div class=\"jsPanel-ftr\"></div>"), t;
  },
  createMinimizedTemplate: function createMinimizedTemplate() {
    var e = document.createElement("div");
    return e.className = "jsPanel-replacement", e.innerHTML = "<div class=\"jsPanel-hdr\">\n                                <div class=\"jsPanel-headerbar\">\n                                    <div class=\"jsPanel-headerlogo\"></div>\n                                    <div class=\"jsPanel-titlebar\">\n                                        <span class=\"jsPanel-title\"></span>\n                                    </div>\n                                    <div class=\"jsPanel-controlbar\">\n                                        <div class=\"jsPanel-btn jsPanel-btn-md jsPanel-btn-normalize\">".concat(this.icons.normalize, "</div>\n                                        <div class=\"jsPanel-btn jsPanel-btn-md jsPanel-btn-maximize\">").concat(this.icons.maximize, "</div>\n                                        <div class=\"jsPanel-btn jsPanel-btn-md jsPanel-btn-close\">").concat(this.icons.close, "</div>\n                                    </div>\n                                </div>\n                            </div>"), e;
  },
  createSnapArea: function createSnapArea(e, t, n) {
    var o = document.createElement("div"),
        a = e.parentElement;
    o.className = "jsPanel-snap-area jsPanel-snap-area-".concat(t), "lt" === t || "rt" === t || "rb" === t || "lb" === t ? (o.style.width = n + "px", o.style.height = n + "px") : "ct" === t || "cb" === t ? o.style.height = n + "px" : "lc" !== t && "rc" !== t || (o.style.width = n + "px"), a !== document.body && (o.style.position = "absolute"), document.querySelector(".jsPanel-snap-area.jsPanel-snap-area-".concat(t)) || e.parentElement.appendChild(o);
  },
  dragit: function dragit(e) {
    var t = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var n, o;
    var a = new CustomEvent("jspaneldragstart", {
      detail: e.id
    }),
        i = new CustomEvent("jspaneldrag", {
      detail: e.id
    }),
        s = new CustomEvent("jspaneldragstop", {
      detail: e.id
    }),
        l = Object.assign({}, this.defaults.dragit, t),
        r = this.pOcontainment(l.containment);
    return l.grid && Array.isArray(l.grid) && 1 === l.grid.length && (l.grid[1] = l.grid[0]), e.querySelectorAll(l.handles).forEach(function (t) {
      t.style.touchAction = "none", t.style.cursor = l.cursor, jsPanel.pointerdown.forEach(function (s) {
        t.addEventListener(s, function (t) {
          if (t.preventDefault(), t.button && t.button > 0) return !1;
          if (t.target.closest(".jsPanel-ftr-btn")) return;
          e.controlbar.style.pointerEvents = "none", e.content.style.pointerEvents = "none";
          var s = window.getComputedStyle(e),
              c = parseFloat(s.left),
              d = parseFloat(s.top),
              p = t.touches ? t.touches[0].clientX : t.clientX,
              h = t.touches ? t.touches[0].clientY : t.clientY,
              f = e.parentElement,
              m = f.getBoundingClientRect(),
              g = window.getComputedStyle(f),
              u = e.getScaleFactor(),
              b = 0;
          o = function o(t) {
            if (t.preventDefault(), !n) {
              if (document.dispatchEvent(a), e.style.opacity = l.opacity, e.snapped && l.snap.resizeToPreSnap && e.currentData.beforeSnap) {
                e.resize(e.currentData.beforeSnap.width + " " + e.currentData.beforeSnap.height);

                var _t2 = e.getBoundingClientRect(),
                    _n = p - (_t2.left + _t2.width),
                    _o = _t2.width / 2;

                _n > -_o && (b = _n + _o);
              }

              l.start.length && jsPanel.processCallbacks(e, l.start, !1, {
                left: c,
                top: d
              }, t), jsPanel.front(e), e.snapped = !1;
            }

            if (n = 1, l.disableOnMaximized && "maximized" === e.status) return !1;
            var o,
                s,
                y,
                w,
                j,
                P,
                v,
                C,
                E,
                F,
                x = t.touches ? t.touches[0].clientX : t.clientX,
                z = t.touches ? t.touches[0].clientY : t.clientY,
                S = window.getComputedStyle(e);

            if (f === document.body) {
              var _t3 = e.getBoundingClientRect();

              E = window.innerWidth - parseInt(g.borderLeftWidth, 10) - parseInt(g.borderRightWidth, 10) - (_t3.left + _t3.width), F = window.innerHeight - parseInt(g.borderTopWidth, 10) - parseInt(g.borderBottomWidth, 10) - (_t3.top + _t3.height);
            } else E = parseInt(g.width, 10) - parseInt(g.borderLeftWidth, 10) - parseInt(g.borderRightWidth, 10) - (parseInt(S.left, 10) + parseInt(S.width, 10)), F = parseInt(g.height, 10) - parseInt(g.borderTopWidth, 10) - parseInt(g.borderBottomWidth, 10) - (parseInt(S.top, 10) + parseInt(S.height, 10));

            if (o = parseFloat(S.left), y = parseFloat(S.top), j = E, v = F, l.snap) if ("panel" === l.snap.trigger) s = Math.pow(o, 2), w = Math.pow(y, 2), P = Math.pow(j, 2), C = Math.pow(v, 2);else if ("pointer" === l.snap.trigger) if ("window" === e.options.container) o = x, s = Math.pow(x, 2), w = Math.pow(y = z, 2), P = Math.pow(j = window.innerWidth - x, 2), C = Math.pow(v = window.innerHeight - z, 2);else {
              var _n2 = jsPanel.overlaps(e, f, "paddingbox", t);

              o = _n2.pointer.left, y = _n2.pointer.top, j = _n2.pointer.right, v = _n2.pointer.bottom, s = Math.pow(_n2.pointer.left, 2), w = Math.pow(_n2.pointer.top, 2), P = Math.pow(_n2.pointer.right, 2), C = Math.pow(_n2.pointer.bottom, 2);
            }
            var B = Math.sqrt(s + w),
                A = Math.sqrt(s + C),
                k = Math.sqrt(P + w),
                $ = Math.sqrt(P + C),
                T = Math.abs(o - j) / 2,
                L = Math.abs(y - v) / 2,
                D = Math.sqrt(s + Math.pow(L, 2)),
                R = Math.sqrt(w + Math.pow(T, 2)),
                q = Math.sqrt(P + Math.pow(L, 2)),
                O = Math.sqrt(C + Math.pow(T, 2));

            if (window.getSelection().removeAllRanges(), document.dispatchEvent(i), l.axis && "x" !== l.axis || (e.style.left = c + (x - p) / u.x + b + "px"), l.axis && "y" !== l.axis || (e.style.top = d + (z - h) / u.y + "px"), l.grid) {
              var _t4 = l.grid[0] * Math.round((c + (x - p)) / l.grid[0]),
                  _n3 = l.grid[1] * Math.round((d + (z - h)) / l.grid[1]);

              e.style.left = "".concat(_t4, "px"), e.style.top = "".concat(_n3, "px");
            }

            if (l.containment || 0 === l.containment) {
              var _t5, _n4;

              if (e.options.container === document.body) _t5 = window.innerWidth - parseFloat(S.width) - r[1], _n4 = window.innerHeight - parseFloat(S.height) - r[2];else {
                var _e3 = parseFloat(g.borderLeftWidth) + parseFloat(g.borderRightWidth),
                    _o2 = parseFloat(g.borderTopWidth) + parseFloat(g.borderBottomWidth);

                _t5 = m.width / u.x - parseFloat(S.width) - r[1] - _e3, _n4 = m.height / u.y - parseFloat(S.height) - r[2] - _o2;
              }
              parseFloat(e.style.left) <= r[3] && (e.style.left = r[3] + "px"), parseFloat(e.style.top) <= r[0] && (e.style.top = r[0] + "px"), parseFloat(e.style.left) >= _t5 && (e.style.left = _t5 + "px"), parseFloat(e.style.top) >= _n4 && (e.style.top = _n4 + "px");
            }

            if (l.drag.length && jsPanel.processCallbacks(e, l.drag, !1, {
              left: o,
              top: y,
              right: j,
              bottom: v
            }, t), l.snap) {
              var _t6 = l.snap.sensitivity,
                  _n5 = f === document.body ? window.innerWidth / 8 : m.width / 8,
                  _a2 = f === document.body ? window.innerHeight / 8 : m.height / 8;

              e.snappableTo = !1, jsPanel.removeSnapAreas(), B < _t6 ? (e.snappableTo = "left-top", !1 !== l.snap.snapLeftTop && jsPanel.createSnapArea(e, "lt", _t6)) : A < _t6 ? (e.snappableTo = "left-bottom", !1 !== l.snap.snapLeftBottom && jsPanel.createSnapArea(e, "lb", _t6)) : k < _t6 ? (e.snappableTo = "right-top", !1 !== l.snap.snapRightTop && jsPanel.createSnapArea(e, "rt", _t6)) : $ < _t6 ? (e.snappableTo = "right-bottom", !1 !== l.snap.snapRightBottom && jsPanel.createSnapArea(e, "rb", _t6)) : y < _t6 && R < _n5 ? (e.snappableTo = "center-top", !1 !== l.snap.snapCenterTop && jsPanel.createSnapArea(e, "ct", _t6)) : o < _t6 && D < _a2 ? (e.snappableTo = "left-center", !1 !== l.snap.snapLeftCenter && jsPanel.createSnapArea(e, "lc", _t6)) : j < _t6 && q < _a2 ? (e.snappableTo = "right-center", !1 !== l.snap.snapRightCenter && jsPanel.createSnapArea(e, "rc", _t6)) : v < _t6 && O < _n5 && (e.snappableTo = "center-bottom", !1 !== l.snap.snapCenterBottom && jsPanel.createSnapArea(e, "cb", _t6));
            }
          }, jsPanel.pointermove.forEach(function (e) {
            document.addEventListener(e, o);
          }), window.addEventListener("mouseout", function (t) {
            null === t.relatedTarget && jsPanel.pointermove.forEach(function (t) {
              document.removeEventListener(t, o, !1), e.style.opacity = 1;
            });
          }, !1);
        });
      }), jsPanel.pointerup.forEach(function (t) {
        document.addEventListener(t, function (t) {
          jsPanel.pointermove.forEach(function (e) {
            document.removeEventListener(e, o);
          }), document.body.style.overflow = "inherit", jsPanel.removeSnapAreas(), n && (e.style.opacity = 1, n = void 0, e.saveCurrentPosition(), e.calcSizeFactors(), document.dispatchEvent(s), l.snap && ("left-top" === e.snappableTo ? jsPanel.snapPanel(e, l.snap.snapLeftTop) : "center-top" === e.snappableTo ? jsPanel.snapPanel(e, l.snap.snapCenterTop) : "right-top" === e.snappableTo ? jsPanel.snapPanel(e, l.snap.snapRightTop) : "right-center" === e.snappableTo ? jsPanel.snapPanel(e, l.snap.snapRightCenter) : "right-bottom" === e.snappableTo ? jsPanel.snapPanel(e, l.snap.snapRightBottom) : "center-bottom" === e.snappableTo ? jsPanel.snapPanel(e, l.snap.snapCenterBottom) : "left-bottom" === e.snappableTo ? jsPanel.snapPanel(e, l.snap.snapLeftBottom) : "left-center" === e.snappableTo && jsPanel.snapPanel(e, l.snap.snapLeftCenter), l.snap.callback && e.snappableTo && "function" == typeof l.snap.callback && l.snap.callback.call(e, e), e.snappableTo && l.snap.repositionOnSnap && e.repositionOnSnap(e.snappableTo)), l.stop.length && jsPanel.processCallbacks(e, l.stop, !1, {
            left: parseFloat(e.style.left),
            top: parseFloat(e.style.top)
          }, t)), e.controlbar.style.pointerEvents = "inherit", e.content.style.pointerEvents = "inherit";
        });
      }), l.disable && (t.style.pointerEvents = "none");
    }), e;
  },
  emptyNode: function emptyNode(e) {
    for (; e.firstChild;) {
      e.removeChild(e.firstChild);
    }

    return e;
  },
  extend: function extend(e) {
    if ("[object Object]" === Object.prototype.toString.call(e)) for (var t in e) {
      e.hasOwnProperty(t) && (this.extensions[t] = e[t]);
    }
  },
  fetch: function (_fetch) {
    function fetch(_x) {
      return _fetch.apply(this, arguments);
    }

    fetch.toString = function () {
      return _fetch.toString();
    };

    return fetch;
  }(function (obj) {
    var confDefaults = {
      bodyMethod: "text",
      evalscripttags: !0,
      autoresize: !0,
      autoreposition: !0,
      done: function done(e, t) {
        e.content.innerHTML = t;
      }
    },
        conf = "string" == typeof obj.options.contentFetch ? Object.assign({
      resource: obj.options.contentFetch
    }, confDefaults) : Object.assign(confDefaults, obj.options.contentFetch),
        fetchInit = conf.fetchInit || {};

    if (!conf.resource) {
      if (this.errorReporting) try {
        throw new jsPanel.jsPanelError("The Fetch request seems to miss the <code>resource</code> parameter");
      } catch (e) {
        jsPanel.error(e);
      }
      return obj;
    }

    conf.beforeSend && conf.beforeSend.call(obj, obj), fetch(conf.resource, fetchInit).then(function (e) {
      if (e.ok) return e[conf.bodyMethod]();
    }).then(function (response) {
      if (conf.done.call(obj, obj, response), conf.evalscripttags) {
        var scripttags = response.match(/<script\b[^>]*>([\s\S]*?)<\/script>/gi);
        scripttags && scripttags.forEach(function (tag) {
          var js = tag.replace(/<script\b[^>]*>/i, "").replace(/<\/script>/i, "").trim();
          eval(js);
        });
      }

      var oContentSize = obj.options.contentSize;
      if (conf.autoresize || conf.autoreposition) if ("string" == typeof oContentSize && oContentSize.match(/auto/i)) {
        var e = oContentSize.split(" "),
            t = Object.assign({}, {
          width: e[0],
          height: e[1]
        });
        conf.autoresize && obj.resize(t), obj.classList.contains("jsPanel-contextmenu") || conf.autoreposition && obj.reposition();
      } else if ("object" == _typeof(oContentSize) && ("auto" === oContentSize.width || "auto" === oContentSize.height)) {
        var _e4 = Object.assign({}, oContentSize);

        conf.autoresize && obj.resize(_e4), obj.classList.contains("jsPanel-contextmenu") || conf.autoreposition && obj.reposition();
      }
    });
  }),
  front: function front(e) {
    if ("minimized" === e.status) "maximized" === e.statusBefore ? e.maximize() : e.normalize();else {
      var t = Array.prototype.slice.call(document.querySelectorAll(".jsPanel-standard")).map(function (e) {
        return e.style.zIndex;
      });
      Math.max.apply(Math, _toConsumableArray(t)) > e.style.zIndex && (e.style.zIndex = jsPanel.zi.next()), this.resetZi();
    }
    this.getPanels().forEach(function (e, t) {
      var n = e.content.querySelector(".jsPanel-iframe-overlay");

      if (t > 0) {
        if (e.content.querySelector("iframe") && !n) {
          var _t7 = document.createElement("div");

          _t7.className = "jsPanel-iframe-overlay", e.content.appendChild(_t7);
        }
      } else n && e.content.removeChild(n);
    });
  },
  getPanels: function getPanels() {
    var e = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {
      return this.classList.contains("jsPanel-standard");
    };
    return Array.prototype.slice.call(document.querySelectorAll(".jsPanel")).filter(function (t) {
      return e.call(t, t);
    }).sort(function (e, t) {
      return t.style.zIndex - e.style.zIndex;
    });
  },
  overlaps: function overlaps(e, t, n, o) {
    var a = "string" == typeof e ? document.querySelector(e) : e,
        i = a.getBoundingClientRect(),
        s = getComputedStyle(a.parentElement),
        l = a.getScaleFactor(),
        r = {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    };
    var c,
        d = 0,
        p = 0,
        h = 0,
        f = 0;
    "window" !== a.options.container && "paddingbox" === n && (r.top = parseInt(s.borderTopWidth, 10) * l.y, r.right = parseInt(s.borderRightWidth, 10) * l.x, r.bottom = parseInt(s.borderBottomWidth, 10) * l.y, r.left = parseInt(s.borderLeftWidth, 10) * l.x), c = "string" == typeof t ? "window" === t ? {
      left: 0,
      top: 0,
      right: window.innerWidth,
      bottom: window.innerHeight
    } : "parent" === t ? a.parentElement.getBoundingClientRect() : document.querySelector(t).getBoundingClientRect() : t.getBoundingClientRect(), o && (d = o.touches ? o.touches[0].clientX : o.clientX, p = o.touches ? o.touches[0].clientY : o.clientY, h = d - c.left, f = p - c.top);
    var m = i.left < c.right && i.right > c.left,
        g = i.top < c.bottom && i.bottom > c.top;
    return {
      overlaps: m && g,
      top: i.top - c.top - r.top,
      right: c.right - i.right - r.right,
      bottom: c.bottom - i.bottom - r.bottom,
      left: i.left - c.left - r.left,
      parentBorderWidth: r,
      panelRect: i,
      referenceRect: c,
      pointer: {
        clientX: d,
        clientY: p,
        left: h - r.left,
        top: f - r.top,
        right: c.width - h - r.right,
        bottom: c.height - f - r.bottom
      }
    };
  },
  pOcontainer: function pOcontainer(e) {
    if (e) {
      if ("string" == typeof e) return "window" === e ? document.body : document.querySelector(e);
      if (1 === e.nodeType) return e;
      if (e.length) return e[0];
    }

    return !1;
  },
  pOcontainment: function pOcontainment(e) {
    var t = e;
    if ("function" == typeof e && (t = e()), "number" == typeof t) return [t, t, t, t];

    if (Array.isArray(t)) {
      if (1 === t.length) return [t[0], t[0], t[0], t[0]];
      if (2 === t.length) return t.concat(t);
      3 === t.length && (t[3] = t[1]);
    }

    return t;
  },
  pOsize: function pOsize(e, t) {
    var n = t || this.defaults.contentSize;
    var o = e.parentElement;

    if ("string" == typeof n) {
      var _e5 = n.trim().split(" ");

      (n = {}).width = _e5[0], 2 === _e5.length ? n.height = _e5[1] : n.height = _e5[0];
    } else n.width && !n.height ? n.height = n.width : n.height && !n.width && (n.width = n.height);

    if (String(n.width).match(/^[0-9.]+$/gi)) n.width += "px";else if ("string" == typeof n.width && n.width.endsWith("%")) {
      if (o === document.body) n.width = window.innerWidth * (parseFloat(n.width) / 100) + "px";else {
        var _e6 = window.getComputedStyle(o),
            _t8 = parseFloat(_e6.borderLeftWidth) + parseFloat(_e6.borderRightWidth);

        n.width = (parseFloat(_e6.width) - _t8) * (parseFloat(n.width) / 100) + "px";
      }
    } else "function" == typeof n.width && (n.width = n.width.call(e, e), "number" == typeof n.width ? n.width += "px" : "string" == typeof n.width && n.width.match(/^[0-9.]+$/gi) && (n.width += "px"));
    if (String(n.height).match(/^[0-9.]+$/gi)) n.height += "px";else if ("string" == typeof n.height && n.height.endsWith("%")) {
      if (o === document.body) n.height = window.innerHeight * (parseFloat(n.height) / 100) + "px";else {
        var _e7 = window.getComputedStyle(o),
            _t9 = parseFloat(_e7.borderTopWidth) + parseFloat(_e7.borderBottomWidth);

        n.height = (parseFloat(_e7.height) - _t9) * (parseFloat(n.height) / 100) + "px";
      }
    } else "function" == typeof n.height && (n.height = n.height.call(e, e), "number" == typeof n.height ? n.height += "px" : "string" == typeof n.height && n.height.match(/^[0-9.]+$/gi) && (n.height += "px"));
    return n;
  },
  pOborder: function pOborder(e) {
    e = e.trim();
    var t = new Array(3),
        n = e.match(/\s*(none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset)\s*/gi),
        o = e.match(/\s*(thin|medium|thick)|(\d*\.?\d+[a-zA-Z]{2,4})\s*/gi);
    return n ? (t[1] = n[0].trim(), e = e.replace(t[1], "")) : t[1] = "solid", o ? (t[0] = o[0].trim(), e = e.replace(t[0], "")) : t[0] = "medium", t[2] = e.trim(), t;
  },
  pOposition: function pOposition(e) {
    var t = e.trim().toLowerCase();
    var n = {},
        o = /(^|\s)((left-|right-)(top|center|bottom){1})|((center-){1}(top|bottom){1})|(center)/gi,
        a = /(^|\s)(right|down|left|up)/gi,
        i = /(^|\s)-?(\d*\.?\d+)([a-zA-Z]{0,4})/gi,
        s = t.match(o);
    s && (n.my = s[0].trim(), n.at = s[1] ? s[1].trim() : s[0].trim());
    var l = (t = t.replace(o, " ").trim()).match(a);
    l && (n.autoposition = l[0].trim());
    var r = (t = t.replace(a, " ").trim()).match(i);
    return r && (r.forEach(function (e, t) {
      (e = e.trim()).match(/(^|\s)(-?[0-9]*\.?[0-9]+)(\s|$)/) && (r[t] = "".concat(r[t], "px").trim());
    }), n.offsetX = r[0].trim(), n.offsetY = r[1] ? r[1].trim() : r[0].trim()), (t = t.replace(i, " ").trim()).length && (n.of = t), n;
  },
  pOheaderControls: function pOheaderControls(e) {
    if ("string" == typeof e) {
      var t = {},
          n = e.toLowerCase(),
          o = n.match(/xl|lg|md|sm|xs/),
          a = n.match(/closeonly|none/);
      return o && (t.size = o[0]), a && (t = Object.assign({}, t, {
        maximize: "remove",
        normalize: "remove",
        minimize: "remove",
        smallify: "remove",
        smallifyrev: "remove"
      }), "none" === a[0] && (t.close = "remove")), Object.assign({}, this.defaults.headerControls, t);
    }

    return Object.assign({}, this.defaults.headerControls, e);
  },
  position: function position(e, t) {
    var n,
        o,
        a,
        i = 0,
        s = 0,
        l = 0,
        r = 0;
    var c = {
      left: 0,
      top: 0
    },
        d = this.defaults.position,
        p = {
      width: document.documentElement.clientWidth,
      height: window.innerHeight
    };
    if (e.options.container === document.body && (d.of = document.body), n = "string" == typeof e ? document.querySelector(e) : e, !t) return n.style.opacity = 1, n;
    var h = n.getBoundingClientRect();
    "string" == typeof t ? o = Object.assign({}, d, jsPanel.pOposition(t)) : (o = Object.assign({}, d, t), ["my", "at", "of", "offsetX", "offsetY", "minLeft", "maxLeft", "minTop", "maxTop"].forEach(function (t) {
      "function" == typeof o[t] && (o[t] = o[t].call(e, e));
    }));
    var f = n.parentElement,
        m = window.getComputedStyle(f),
        g = f.getBoundingClientRect(),
        u = f.tagName.toLowerCase();
    if (o.of && "window" !== o.of && (a = "string" == typeof o.of ? document.querySelector(o.of) : o.of), o.my.match(/^center-top$|^center$|^center-bottom$/i) ? i = h.width / 2 : o.my.match(/right/i) && (i = h.width), o.my.match(/^left-center$|^center$|^right-center$/i) ? s = h.height / 2 : o.my.match(/bottom/i) && (s = h.height), "window" === e.options.container && "body" === u && "window" === o.of) o.at.match(/^center-top$|^center$|^center-bottom$/i) ? l = p.width / 2 : o.at.match(/right/i) && (l = p.width), o.at.match(/^left-center$|^center$|^right-center$/i) ? r = p.height / 2 : o.at.match(/bottom/i) && (r = p.height), c.left = l - i - parseFloat(m.borderLeftWidth), c.top = r - s - parseFloat(m.borderTopWidth), n.style.position = "fixed";else if ("window" === e.options.container && "body" === u && o.of.classList && o.of.classList.contains("jsPanel")) {
      var _e8 = a.getBoundingClientRect();

      l = o.at.match(/^center-top$|^center$|^center-bottom$/i) ? _e8.width / 2 + _e8.left : o.at.match(/right/i) ? _e8.width + _e8.left : _e8.left, r = o.at.match(/^left-center$|^center$|^right-center$/i) ? _e8.height / 2 + _e8.top : o.at.match(/bottom/i) ? _e8.height + _e8.top : _e8.top, c.left = l - i, c.top = r - s, n.style.position = "fixed";
    } else if ("body" === u && "window" !== o.of) {
      var _e9 = a.getBoundingClientRect();

      l = o.at.match(/^center-top$|^center$|^center-bottom$/i) ? _e9.width / 2 + _e9.left + pageXOffset : o.at.match(/right/i) ? _e9.width + _e9.left + pageXOffset : _e9.left + pageXOffset, r = o.at.match(/^left-center$|^center$|^right-center$/i) ? _e9.height / 2 + _e9.top + pageYOffset : o.at.match(/bottom/i) ? _e9.height + _e9.top + pageYOffset : _e9.top + pageYOffset, c.left = l - i - parseFloat(m.borderLeftWidth), c.top = r - s - parseFloat(m.borderTopWidth);
    } else if ("body" === u || "window" !== o.of && o.of) {
      if ("body" !== u && f.contains(a)) {
        var _e10 = a.getBoundingClientRect();

        l = o.at.match(/^center-top$|^center$|^center-bottom$/i) ? _e10.left - g.left + _e10.width / 2 : o.at.match(/right/i) ? _e10.left - g.left + _e10.width : _e10.left - g.left, r = o.at.match(/^left-center$|^center$|^right-center$/i) ? _e10.top - g.top + _e10.height / 2 : o.at.match(/bottom/i) ? _e10.top - g.top + _e10.height : _e10.top - g.top, c.left = l - i - parseFloat(m.borderLeftWidth), c.top = r - s - parseFloat(m.borderTopWidth);
      }
    } else {
      var _e11 = parseFloat(m.borderLeftWidth) + parseFloat(m.borderRightWidth),
          _t10 = parseFloat(m.borderTopWidth) + parseFloat(m.borderBottomWidth);

      o.at.match(/^center-top$|^center$|^center-bottom$/i) ? l = g.width / 2 - _e11 / 2 : o.at.match(/right/i) && (l = g.width - _e11), o.at.match(/^left-center$|^center$|^right-center$/i) ? r = g.height / 2 - _t10 / 2 : o.at.match(/bottom/i) && (r = g.height - _t10), c.left = l - i, c.top = r - s;
    }

    if (o.autoposition && o.my === o.at && ["left-top", "center-top", "right-top", "left-bottom", "center-bottom", "right-bottom"].indexOf(o.my) >= 0) {
      "function" == typeof o.autoposition && (o.autoposition = o.autoposition());

      var _e12 = "".concat(o.my, "-").concat(o.autoposition.toLowerCase());

      n.classList.add(_e12);

      var _t11 = Array.prototype.slice.call(document.querySelectorAll(".".concat(_e12))),
          _a3 = _t11.indexOf(n);

      _t11.length > 1 && ("down" === o.autoposition ? _t11.forEach(function (e, n) {
        n > 0 && n <= _a3 && (c.top += _t11[--n].getBoundingClientRect().height + jsPanel.autopositionSpacing);
      }) : "up" === o.autoposition ? _t11.forEach(function (e, n) {
        n > 0 && n <= _a3 && (c.top -= _t11[--n].getBoundingClientRect().height + jsPanel.autopositionSpacing);
      }) : "right" === o.autoposition ? _t11.forEach(function (e, n) {
        n > 0 && n <= _a3 && (c.left += _t11[--n].getBoundingClientRect().width + jsPanel.autopositionSpacing);
      }) : "left" === o.autoposition && _t11.forEach(function (e, n) {
        n > 0 && n <= _a3 && (c.left -= _t11[--n].getBoundingClientRect().width + jsPanel.autopositionSpacing);
      }));
    }

    var b = n.getScaleFactor ? n.getScaleFactor() : {
      x: 1,
      y: 1
    };
    c.left /= b.x, c.top /= b.y;
    var y = parseFloat(m.borderLeftWidth) + parseFloat(m.borderRightWidth),
        w = parseFloat(m.borderTopWidth) + parseFloat(m.borderBottomWidth),
        j = y * (1 - b.x) / b.x,
        P = w * (1 - b.y) / b.y;

    if (o.at.match(/^right-top$|^right-center$|^right-bottom$/i) ? c.left += j : o.at.match(/^center-top$|^center$|^center-bottom$/i) && (c.left += j / 2), o.at.match(/^left-bottom$|^center-bottom$|^right-bottom$/i) ? c.top += P : o.at.match(/^left-center$|^center$|^right-center$/i) && (c.top += P / 2), c.left += "px", c.top += "px", n.style.left = c.left, n.style.top = c.top, o.offsetX && ("number" == typeof o.offsetX ? n.style.left = "calc(".concat(c.left, " + ").concat(o.offsetX, "px)") : n.style.left = "calc(".concat(c.left, " + ").concat(o.offsetX, ")"), c.left = window.getComputedStyle(n).left), o.offsetY && ("number" == typeof o.offsetY ? n.style.top = "calc(".concat(c.top, " + ").concat(o.offsetY, "px)") : n.style.top = "calc(".concat(c.top, " + ").concat(o.offsetY, ")"), c.top = window.getComputedStyle(n).top), o.minLeft) {
      var _e13 = parseFloat(c.left);

      "number" == typeof o.minLeft && (o.minLeft += "px"), n.style.left = o.minLeft, _e13 > parseFloat(window.getComputedStyle(n).left) && (n.style.left = _e13 + "px"), c.left = window.getComputedStyle(n).left;
    }

    if (o.maxLeft) {
      var _e14 = parseFloat(c.left);

      "number" == typeof o.maxLeft && (o.maxLeft += "px"), n.style.left = o.maxLeft, _e14 < parseFloat(window.getComputedStyle(n).left) && (n.style.left = _e14 + "px"), c.left = window.getComputedStyle(n).left;
    }

    if (o.maxTop) {
      var _e15 = parseFloat(c.top);

      "number" == typeof o.maxTop && (o.maxTop += "px"), n.style.top = o.maxTop, _e15 < parseFloat(window.getComputedStyle(n).top) && (n.style.top = _e15 + "px"), c.top = window.getComputedStyle(n).top;
    }

    if (o.minTop) {
      var _e16 = parseFloat(c.top);

      "number" == typeof o.minTop && (o.minTop += "px"), n.style.top = o.minTop, _e16 > parseFloat(window.getComputedStyle(n).top) && (n.style.top = _e16 + "px"), c.top = window.getComputedStyle(n).top;
    }

    if ("function" == typeof o.modify) {
      var _e17 = o.modify.call(c, c);

      n.style.left = _e17.left, n.style.top = _e17.top;
    }

    return n.style.opacity = 1, n.style.left = window.getComputedStyle(n).left, n.style.top = window.getComputedStyle(n).top, n;
  },
  processCallbacks: function processCallbacks(e, t) {
    var n = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "some";
    var o = arguments.length > 3 ? arguments[3] : undefined;
    var a = arguments.length > 4 ? arguments[4] : undefined;
    if ("function" == typeof t && (t = [t]), n) return t[n](function (t) {
      return t.call(e, e, o, a);
    });
    t.forEach(function (t) {
      t.call(e, e, o, a);
    });
  },
  removeMinimizedReplacement: function removeMinimizedReplacement(e) {
    var t = document.getElementById("".concat(e.id, "-min"));
    t && t.parentElement.removeChild(t);
  },
  removeSnapAreas: function removeSnapAreas() {
    document.querySelectorAll(".jsPanel-snap-area").forEach(function (e) {
      e.parentElement.removeChild(e);
    });
  },
  remClass: function remClass(e, t) {
    return t.split(" ").forEach(function (t) {
      return e.classList.remove(t);
    }), e;
  },
  resetZi: function resetZi() {
    this.zi = function () {
      var e = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : jsPanel.ziBase;
      var t = e;
      return {
        next: function next() {
          return t++;
        }
      };
    }(), Array.prototype.slice.call(document.querySelectorAll(".jsPanel-standard")).sort(function (e, t) {
      return e.style.zIndex - t.style.zIndex;
    }).forEach(function (e) {
      e.style.zIndex = jsPanel.zi.next();
    });
  },
  resizeit: function resizeit(e) {
    var t = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var n = Object.assign({}, this.defaults.resizeit, t),
        o = e.parentElement,
        a = o.tagName.toLowerCase(),
        i = this.pOcontainment(n.containment),
        s = new CustomEvent("jspanelresizestart", {
      detail: e.id
    }),
        l = new CustomEvent("jspanelresize", {
      detail: e.id
    }),
        r = new CustomEvent("jspanelresizestop", {
      detail: e.id
    });
    var c,
        d,
        p,
        h,
        f = "function" == typeof n.maxWidth ? n.maxWidth() : n.maxWidth || 1e4,
        m = "function" == typeof n.maxHeight ? n.maxHeight() : n.maxHeight || 1e4,
        g = "function" == typeof n.minWidth ? n.minWidth() : n.minWidth,
        u = "function" == typeof n.minHeight ? n.minHeight() : n.minHeight;
    return n.handles.split(",").forEach(function (t) {
      var n = document.createElement("DIV");
      n.className = "jsPanel-resizeit-handle jsPanel-resizeit-".concat(t.trim()), n.style.zIndex = 90, e.append(n);
    }), e.querySelectorAll(".jsPanel-resizeit-handle").forEach(function (t) {
      jsPanel.pointerdown.forEach(function (r) {
        t.addEventListener(r, function (t) {
          if (t.preventDefault(), t.button && t.button > 0) return !1;
          e.content.style.pointerEvents = "none";
          var r = e.getBoundingClientRect(),
              b = o.getBoundingClientRect(),
              y = window.getComputedStyle(o, null),
              w = parseInt(y.borderLeftWidth, 10),
              j = parseInt(y.borderTopWidth, 10),
              P = y.getPropertyValue("position"),
              v = t.clientX || t.touches[0].clientX,
              C = t.clientY || t.touches[0].clientY,
              E = v / C,
              F = r.width,
              x = r.height,
              z = t.target.classList,
              S = e.getScaleFactor(),
              B = r.width / r.height;
          var A = r.left,
              k = r.top,
              $ = 1e4,
              T = 1e4,
              L = 1e4,
              D = 1e4;
          "body" !== a && (A = r.left - b.left + o.scrollLeft, k = r.top - b.top + o.scrollTop), "body" === a && i ? ($ = document.documentElement.clientWidth - r.left, L = document.documentElement.clientHeight - r.top, T = r.width + r.left, D = r.height + r.top) : i && ("static" === P ? ($ = b.width - r.left + w, L = b.height + b.top - r.top + j, T = r.width + (r.left - b.left) - w, D = r.height + (r.top - b.top) - j) : ($ = o.clientWidth - (r.left - b.left) / S.x + w, L = o.clientHeight - (r.top - b.top) / S.y + j, T = (r.width + r.left - b.left) / S.x - w, D = e.clientHeight + (r.top - b.top) / S.y - j)), i && (T -= i[3], D -= i[0], $ -= i[1], L -= i[2]);
          var R = window.getComputedStyle(e),
              q = parseFloat(R.width) - r.width,
              O = parseFloat(R.height) - r.height;
          var W = parseFloat(R.left) - r.left,
              I = parseFloat(R.top) - r.top;
          o !== document.body && (W += b.left, I += b.top), c = function c(t) {
            d || (document.dispatchEvent(s), n.start.length && jsPanel.processCallbacks(e, n.start, !1, {
              width: F,
              height: x
            }, t), jsPanel.front(e)), d = 1, document.dispatchEvent(l);
            var a,
                r = t.touches ? t.touches[0].clientX : t.clientX,
                c = t.touches ? t.touches[0].clientY : t.clientY;
            z.contains("jsPanel-resizeit-e") ? ((p = F + (r - v) / S.x + q) >= $ && (p = $), p >= f ? p = f : p <= g && (p = g), e.style.width = p + "px", n.aspectRatio && (e.style.height = p / B + "px", n.containment && (a = e.overlaps(o)).bottom <= i[2] && (e.style.height = L + "px", e.style.width = L * B + "px"))) : z.contains("jsPanel-resizeit-s") ? ((h = x + (c - C) / S.y + O) >= L && (h = L), h >= m ? h = m : h <= u && (h = u), e.style.height = h + "px", n.aspectRatio && (e.style.width = h * B + "px", n.containment && (a = e.overlaps(o)).right <= i[1] && (e.style.width = $ + "px", e.style.height = $ / B + "px"))) : z.contains("jsPanel-resizeit-w") ? ((p = F + (v - r) / S.x + q) <= f && p >= g && p <= T && (e.style.left = A + (r - v) / S.x + W + "px"), p >= T && (p = T), p >= f ? p = f : p <= g && (p = g), e.style.width = p + "px", n.aspectRatio && (e.style.height = p / B + "px", n.containment && (a = e.overlaps(o)).bottom <= i[2] && (e.style.height = L + "px", e.style.width = L * B + "px"))) : z.contains("jsPanel-resizeit-n") ? ((h = x + (C - c) / S.y + O) <= m && h >= u && h <= D && (e.style.top = k + (c - C) / S.y + I + "px"), h >= D && (h = D), h >= m ? h = m : h <= u && (h = u), e.style.height = h + "px", n.aspectRatio && (e.style.width = h * B + "px", n.containment && (a = e.overlaps(o)).right <= i[1] && (e.style.width = $ + "px", e.style.height = $ / B + "px"))) : z.contains("jsPanel-resizeit-se") ? ((p = F + (r - v) / S.x + q) >= $ && (p = $), p >= f ? p = f : p <= g && (p = g), e.style.width = p + "px", n.aspectRatio && (e.style.height = p / B + "px"), (h = x + (c - C) / S.y + O) >= L && (h = L), h >= m ? h = m : h <= u && (h = u), e.style.height = h + "px", n.aspectRatio && (e.style.width = h * B + "px", n.containment && (a = e.overlaps(o)).right <= i[1] && (e.style.width = $ + "px", e.style.height = $ / B + "px"))) : z.contains("jsPanel-resizeit-sw") ? ((h = x + (c - C) / S.y + O) >= L && (h = L), h >= m ? h = m : h <= u && (h = u), e.style.height = h + "px", n.aspectRatio && (e.style.width = h * B + "px"), (p = F + (v - r) / S.x + q) <= f && p >= g && p <= T && (e.style.left = A + (r - v) / S.x + W + "px"), p >= T && (p = T), p >= f ? p = f : p <= g && (p = g), e.style.width = p + "px", n.aspectRatio && (e.style.height = p / B + "px", n.containment && (a = e.overlaps(o)).bottom <= i[2] && (e.style.height = L + "px", e.style.width = L * B + "px"))) : z.contains("jsPanel-resizeit-ne") ? ((p = F + (r - v) / S.x + q) >= $ && (p = $), p >= f ? p = f : p <= g && (p = g), e.style.width = p + "px", n.aspectRatio && (e.style.height = p / B + "px"), (h = x + (C - c) / S.y + O) <= m && h >= u && h <= D && (e.style.top = k + (c - C) / S.y + I + "px"), h >= D && (h = D), h >= m ? h = m : h <= u && (h = u), e.style.height = h + "px", n.aspectRatio && (e.style.width = h * B + "px", n.containment && (a = e.overlaps(o)).right <= i[1] && (e.style.width = $ + "px", e.style.height = $ / B + "px"))) : z.contains("jsPanel-resizeit-nw") && (n.aspectRatio && z.contains("jsPanel-resizeit-nw") && (c = (r = c * E) / E), (p = F + (v - r) / S.x + q) <= f && p >= g && p <= T && (e.style.left = A + (r - v) / S.x + W + "px"), p >= T && (p = T), p >= f ? p = f : p <= g && (p = g), e.style.width = p + "px", n.aspectRatio && (e.style.height = p / B + "px"), (h = x + (C - c) / S.y + O) <= m && h >= u && h <= D && (e.style.top = k + (c - C) / S.y + I + "px"), h >= D && (h = D), h >= m ? h = m : h <= u && (h = u), e.style.height = h + "px", n.aspectRatio && (e.style.width = h * B + "px")), window.getSelection().removeAllRanges();
            var b = window.getComputedStyle(e),
                y = {
              left: parseFloat(b.left),
              top: parseFloat(b.top),
              right: parseFloat(b.right),
              bottom: parseFloat(b.bottom),
              width: parseFloat(b.width),
              height: parseFloat(b.height)
            };
            n.resize.length && jsPanel.processCallbacks(e, n.resize, !1, y, t);
          }, jsPanel.pointermove.forEach(function (e) {
            document.addEventListener(e, c, !1);
          }), window.addEventListener("mouseout", function (e) {
            null === e.relatedTarget && jsPanel.pointermove.forEach(function (e) {
              document.removeEventListener(e, c, !1);
            });
          }, !1);
        });
      });
    }), jsPanel.pointerup.forEach(function (t) {
      document.addEventListener(t, function (t) {
        if (jsPanel.pointermove.forEach(function (e) {
          document.removeEventListener(e, c, !1);
        }), t.target.classList && t.target.classList.contains("jsPanel-resizeit-handle")) {
          var _o3,
              _a4,
              _i = t.target.className;

          if (_i.match(/jsPanel-resizeit-nw|jsPanel-resizeit-w|jsPanel-resizeit-sw/i) && (_o3 = !0), _i.match(/jsPanel-resizeit-nw|jsPanel-resizeit-n|jsPanel-resizeit-ne/i) && (_a4 = !0), n.grid && Array.isArray(n.grid)) {
            1 === n.grid.length && (n.grid[1] = n.grid[0]);

            var _t12 = parseFloat(e.style.width),
                _i2 = parseFloat(e.style.height),
                _s = _t12 % n.grid[0],
                _l = _i2 % n.grid[1],
                _r = parseFloat(e.style.left),
                _c = parseFloat(e.style.top),
                _d = _r % n.grid[0],
                _p = _c % n.grid[1];

            _s < n.grid[0] / 2 ? e.style.width = _t12 - _s + "px" : e.style.width = _t12 + (n.grid[0] - _s) + "px", _l < n.grid[1] / 2 ? e.style.height = _i2 - _l + "px" : e.style.height = _i2 + (n.grid[1] - _l) + "px", _o3 && (_d < n.grid[0] / 2 ? e.style.left = _r - _d + "px" : e.style.left = _r + (n.grid[0] - _d) + "px"), _a4 && (_p < n.grid[1] / 2 ? e.style.top = _c - _p + "px" : e.style.top = _c + (n.grid[1] - _p) + "px");
          }
        }

        d && (e.content.style.pointerEvents = "inherit", d = void 0, e.saveCurrentDimensions(), e.saveCurrentPosition(), document.dispatchEvent(r), n.stop.length && jsPanel.processCallbacks(e, n.stop, !1, {
          width: parseFloat(e.style.width),
          height: parseFloat(e.style.height)
        }, t)), e.content.style.pointerEvents = "inherit";
      }, !1);
    }), n.disable && e.querySelectorAll(".jsPanel-resizeit-handle").forEach(function (e) {
      e.style.pointerEvents = "none";
    }), e;
  },
  setClass: function setClass(e, t) {
    return t.split(" ").forEach(function (t) {
      return e.classList.add(t);
    }), e;
  },
  setStyles: function setStyles(e, t) {
    for (var n in t) {
      if (t.hasOwnProperty(n)) {
        var o = String(n).replace(/-\w/gi, function (e) {
          return e.substr(-1).toUpperCase();
        });
        e.style[o] = t[n];
      }
    }

    return e;
  },
  setStyle: function setStyle(e, t) {
    return this.setStyles.call(e, e, t);
  },
  snapPanel: function snapPanel(e, t) {
    var n = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : !1;
    if (n || (e.currentData.beforeSnap = {
      width: e.currentData.width,
      height: e.currentData.height
    }), t && "function" == typeof t && !n) t.call(e, e, e.snappableTo);else if (!1 !== t) {
      var _t13 = [0, 0];

      if (e.options.dragit.snap.containment && e.options.dragit.containment) {
        var _n6 = this.pOcontainment(e.options.dragit.containment),
            o = e.snappableTo;

        o.startsWith("left") ? _t13[0] = _n6[3] : o.startsWith("right") && (_t13[0] = -_n6[1]), o.endsWith("top") ? _t13[1] = _n6[0] : o.endsWith("bottom") && (_t13[1] = -_n6[2]);
      }

      e.reposition("".concat(e.snappableTo, " ").concat(_t13[0], " ").concat(_t13[1]));
    }
    n || (e.snapped = e.snappableTo);
  },
  jsPanelError: function jsPanelError(e) {
    this.prototype = new Error(), this.message = e, this.name = "jsPanel Error";
  },
  error: function error(e) {
    this.create({
      contentSize: {
        width: "calc(100% - 10px)",
        height: "auto"
      },
      position: "center-top 0 5 down",
      animateIn: "jsPanelFadeIn",
      theme: "danger filledlight",
      border: "1px",
      borderRadius: "4px",
      header: !1,
      content: "<div class=\"jsPanel-error-content\"><div class=\"jsPanel-error-content-left\"><div><i class=\"fa fa-exclamation-triangle fa-2x\"></i></div></div><div class=\"jsPanel-error-content-right\"><p>".concat(e.message, "</p></div></div><div class=\"jsPanel-error-close\"><span class=\"jsPanel-ftr-btn\">").concat(jsPanel.icons.close, "</span></div>"),
      callback: function callback(e) {
        e.content.querySelector(".jsPanel-ftr-btn").addEventListener("click", function () {
          e.close();
        });
      }
    });
  },
  create: function create() {
    var _this = this;

    var e = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var t = arguments.length > 1 ? arguments[1] : undefined;
    jsPanel.zi || (jsPanel.zi = function () {
      var e = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : jsPanel.ziBase;
      var t = e;
      return {
        next: function next() {
          return t++;
        }
      };
    }()), e.config ? delete (e = Object.assign({}, this.defaults, e.config, e)).config : e = Object.assign({}, this.defaults, e), e.id ? "function" == typeof e.id && (e.id = e.id()) : e.id = "jsPanel-".concat(jsPanel.idCounter += 1);
    var n = document.getElementById(e.id);

    if (null !== n) {
      if (n.classList.contains("jsPanel") && n.front(), this.errorReporting) try {
        throw new jsPanel.jsPanelError("&#9664; COULD NOT CREATE NEW JSPANEL &#9658;<br>An element with the ID <code>" + e.id + "</code> already exists in the document.");
      } catch (e) {
        jsPanel.error(e);
      }
      return !1;
    }

    var o = this.pOcontainer(e.container);

    if (!o) {
      if (this.errorReporting) try {
        throw new jsPanel.jsPanelError("&#9664; COULD NOT CREATE NEW JSPANEL &#9658;<br>The container to append the panel to does not exist");
      } catch (e) {
        jsPanel.error(e);
      }
      return !1;
    }

    e.maximizedMargin = this.pOcontainment(e.maximizedMargin), e.dragit && (["start", "drag", "stop"].forEach(function (t) {
      e.dragit[t] ? "function" == typeof e.dragit[t] && (e.dragit[t] = [e.dragit[t]]) : e.dragit[t] = [];
    }), e.dragit.snap && ("object" == _typeof(e.dragit.snap) ? e.dragit.snap = Object.assign({}, this.defaultSnapConfig, e.dragit.snap) : e.dragit.snap = this.defaultSnapConfig)), e.resizeit && ["start", "resize", "stop"].forEach(function (t) {
      e.resizeit[t] ? "function" == typeof e.resizeit[t] && (e.resizeit[t] = [e.resizeit[t]]) : e.resizeit[t] = [];
    }), ["onbeforeclose", "onbeforemaximize", "onbeforeminimize", "onbeforenormalize", "onbeforesmallify", "onbeforeunsmallify", "onclosed", "onfronted", "onmaximized", "onminimized", "onnormalized", "onsmallified", "onstatuschange", "onunsmallified"].forEach(function (t) {
      e[t] ? "function" == typeof e[t] && (e[t] = [e[t]]) : e[t] = [];
    });
    var a = e.template ? e.template : this.createPanelTemplate();
    a.options = e, a.closetimer = void 0, a.status = "initialized", a.currentData = {}, a.header = a.querySelector(".jsPanel-hdr"), a.headerbar = a.header.querySelector(".jsPanel-headerbar"), a.titlebar = a.header.querySelector(".jsPanel-titlebar"), a.headerlogo = a.headerbar.querySelector(".jsPanel-headerlogo"), a.headertitle = a.headerbar.querySelector(".jsPanel-title"), a.controlbar = a.headerbar.querySelector(".jsPanel-controlbar"), a.headertoolbar = a.header.querySelector(".jsPanel-hdr-toolbar"), a.content = a.querySelector(".jsPanel-content"), a.footer = a.querySelector(".jsPanel-ftr"), a.snappableTo = !1, a.snapped = !1;
    var i = new CustomEvent("jspanelloaded", {
      detail: e.id
    }),
        s = new CustomEvent("jspanelcloseduser", {
      detail: e.id
    }),
        l = new CustomEvent("jspanelstatuschange", {
      detail: e.id
    }),
        r = new CustomEvent("jspanelbeforenormalize", {
      detail: e.id
    }),
        c = new CustomEvent("jspanelnormalized", {
      detail: e.id
    }),
        d = new CustomEvent("jspanelbeforemaximize", {
      detail: e.id
    }),
        p = new CustomEvent("jspanelmaximized", {
      detail: e.id
    }),
        h = new CustomEvent("jspanelbeforeminimize", {
      detail: e.id
    }),
        f = new CustomEvent("jspanelminimized", {
      detail: e.id
    }),
        m = new CustomEvent("jspanelbeforesmallify", {
      detail: e.id
    }),
        g = new CustomEvent("jspanelsmallified", {
      detail: e.id
    }),
        u = new CustomEvent("jspanelsmallifiedmax", {
      detail: e.id
    }),
        b = new CustomEvent("jspanelbeforeunsmallify", {
      detail: e.id
    }),
        y = new CustomEvent("jspanelfronted", {
      detail: e.id
    }),
        w = a.querySelector(".jsPanel-btn-close"),
        j = a.querySelector(".jsPanel-btn-maximize"),
        P = a.querySelector(".jsPanel-btn-normalize"),
        v = a.querySelector(".jsPanel-btn-smallify"),
        C = a.querySelector(".jsPanel-btn-smallifyrev"),
        E = a.querySelector(".jsPanel-btn-minimize");
    "onpointerdown" in window && a.controlbar.querySelectorAll(".jsPanel-btn").forEach(function (e) {
      e.addEventListener("pointerdown", function (e) {
        e.preventDefault();
      }, !0);
    }), w && jsPanel.pointerup.forEach(function (e) {
      w.addEventListener(e, function (e) {
        if (e.preventDefault(), e.button && e.button > 0) return !1;
        jsPanel.close(a), document.dispatchEvent(s);
      });
    }), j && jsPanel.pointerup.forEach(function (e) {
      j.addEventListener(e, function (e) {
        if (e.preventDefault(), e.button && e.button > 0) return !1;
        a.maximize();
      });
    }), P && jsPanel.pointerup.forEach(function (e) {
      P.addEventListener(e, function (e) {
        if (e.preventDefault(), e.button && e.button > 0) return !1;
        a.normalize();
      });
    }), v && jsPanel.pointerup.forEach(function (e) {
      v.addEventListener(e, function (e) {
        if (e.preventDefault(), e.button && e.button > 0) return !1;
        a.smallify();
      });
    }), C && jsPanel.pointerup.forEach(function (e) {
      C.addEventListener(e, function (e) {
        if (e.preventDefault(), e.button && e.button > 0) return !1;
        a.unsmallify();
      });
    }), E && jsPanel.pointerup.forEach(function (e) {
      E.addEventListener(e, function (e) {
        if (e.preventDefault(), e.button && e.button > 0) return !1;
        a.minimize();
      });
    });
    var F = jsPanel.extensions;

    for (var _e18 in F) {
      F.hasOwnProperty(_e18) && (a[_e18] = F[_e18]);
    }

    if (a.clearTheme = function (e) {
      return jsPanel.themes.forEach(function (e) {
        ["panel", "jsPanel-theme-".concat(e), "panel-".concat(e), "".concat(e, "-color")].forEach(function (e) {
          a.classList.remove(e);
        }), a.header.classList.remove("jsPanel-theme-".concat(e));
      }, a), a.content.classList.remove("jsPanel-content-filled", "jsPanel-content-filledlight"), a.footer.classList.remove("panel-footer"), a.header.classList.remove("jsPanel-hdr-light"), a.header.classList.remove("jsPanel-hdr-dark"), jsPanel.setStyle(a, {
        backgroundColor: "",
        borderWidth: "",
        borderStyle: "",
        borderColor: ""
      }), jsPanel.setStyle(a.content, {
        background: "",
        border: ""
      }), jsPanel.setStyle(a.headertoolbar, {
        boxShadow: "",
        width: "",
        marginLeft: ""
      }), a.header.style.background = "", Array.prototype.slice.call(a.controlbar.querySelectorAll(".jsPanel-icon")).concat([a.headerlogo, a.headertitle, a.headertoolbar, a.content]).forEach(function (e) {
        e.style.color = "";
      }), e && e.call(a, a), a;
    }, a.getThemeDetails = function (e) {
      var t = e.toLowerCase(),
          n = {
        color: !1,
        colors: !1,
        filling: !1
      },
          o = t.split("fill");
      if (n.color = o[0].trim().replace(/\s*/g, ""), 2 === o.length) if (o[1].startsWith("edlight")) n.filling = "filledlight";else if (o[1].startsWith("eddark")) n.filling = "filleddark";else if (o[1].startsWith("ed")) n.filling = "filled";else if (o[1].startsWith("color")) {
        var _e19 = o[1].split("color"),
            _t14 = _e19[_e19.length - 1].trim().replace(/\s*/g, "");

        jsPanel.colorNames[_t14] && (_t14 = jsPanel.colorNames[_t14]), _t14.match(/^([0-9a-f]{3}|[0-9a-f]{6})$/gi) && (_t14 = "#" + _t14), n.filling = _t14;
      }

      if (jsPanel.themes.some(function (e) {
        return e === n.color.split(/\s/i)[0];
      })) {
        var _e20 = n.color.split(/\s/i)[0],
            _t15 = document.createElement("button");

        _t15.className = _e20 + "-bg", document.body.appendChild(_t15), n.color = getComputedStyle(_t15).backgroundColor.replace(/\s+/gi, ""), document.body.removeChild(_t15), _t15 = void 0;
      } else if (n.color.startsWith("bootstrap-")) {
        var _e21 = n.color.indexOf("-"),
            _t16 = document.createElement("button");

        _t16.className = "btn btn" + n.color.slice(_e21), document.body.appendChild(_t16), n.color = getComputedStyle(_t16).backgroundColor.replace(/\s+/gi, ""), document.body.removeChild(_t16), _t16 = void 0;
      } else if (n.color.startsWith("mdb-")) {
        var _e22,
            _t17 = n.color.indexOf("-") + 1,
            _o4 = document.createElement("span");

        _e22 = n.color.endsWith("-dark") ? (_e22 = n.color.slice(_t17)).replace("-dark", "-color-dark") : n.color.slice(_t17) + "-color", _o4.className = _e22, document.body.appendChild(_o4), n.color = getComputedStyle(_o4).backgroundColor.replace(/\s+/gi, ""), document.body.removeChild(_o4), _o4 = void 0;
      }

      return n.colors = jsPanel.calcColors(n.color), n;
    }, a.applyColorTheme = function (e) {
      a.style.backgroundColor = e.colors[0], a.header.style.backgroundColor = e.colors[0], [".jsPanel-headerlogo", ".jsPanel-title", ".jsPanel-hdr-toolbar"].forEach(function (t) {
        a.querySelector(t).style.color = e.colors[3];
      }, a), a.querySelectorAll(".jsPanel-controlbar .jsPanel-btn").forEach(function (t) {
        t.style.color = e.colors[3];
      });
      var t = "#000000" === e.colors[3] ? "1px solid rgba(0,0,0,0.2)" : "1px solid rgba(255,255,255,0.2)";

      if (a.headertoolbar.style.borderTop = t, a.content.style.borderTop = t, "#000000" === e.colors[3] ? a.header.classList.add("jsPanel-hdr-light") : a.header.classList.add("jsPanel-hdr-dark"), e.filling) {
        var _t18 = e.filling;
        if ("filled" === _t18) jsPanel.setStyle(a.content, {
          backgroundColor: e.colors[2],
          color: e.colors[3]
        });else if ("filledlight" === _t18) a.content.style.backgroundColor = e.colors[1];else if ("filleddark" === _t18) jsPanel.setStyle(a.content, {
          backgroundColor: e.colors[6],
          color: e.colors[7]
        });else {
          var _e23 = jsPanel.perceivedBrightness(_t18) <= jsPanel.colorBrightnessThreshold ? "#fff" : "#000";

          a.content.style.backgroundColor = _t18, a.content.style.color = _e23;
        }
      }

      return a;
    }, a.applyCustomTheme = function (e) {
      var t,
          n = {
        bgPanel: "#fff",
        bgContent: "#fff",
        colorHeader: "#000",
        colorContent: "#000"
      },
          o = (t = "object" == _typeof(e) ? Object.assign(n, e) : n).bgPanel,
          i = t.bgContent,
          s = t.colorHeader,
          l = t.colorContent;

      if (jsPanel.colorNames[o] ? a.style.background = "#" + jsPanel.colorNames[o] : a.style.background = o, jsPanel.colorNames[s] && (s = "#" + jsPanel.colorNames[s]), [".jsPanel-headerlogo", ".jsPanel-title", ".jsPanel-hdr-toolbar"].forEach(function (e) {
        a.querySelector(e).style.color = s;
      }, a), a.querySelectorAll(".jsPanel-controlbar .jsPanel-btn").forEach(function (e) {
        e.style.color = s;
      }), jsPanel.colorNames[i] ? a.content.style.background = "#" + jsPanel.colorNames[i] : a.content.style.background = i, jsPanel.colorNames[l] ? a.content.style.color = "#" + jsPanel.colorNames[l] : a.content.style.color = l, jsPanel.perceivedBrightness(s) > jsPanel.colorBrightnessThreshold ? (a.headertoolbar.style.borderTop = "1px solid rgba(255,255,255,0.2)", a.header.classList.add("jsPanel-hdr-dark")) : (a.headertoolbar.style.borderTop = "1px solid rgba(0,0,0,0.2)", a.header.classList.add("jsPanel-hdr-light")), jsPanel.perceivedBrightness(l) > jsPanel.colorBrightnessThreshold ? a.content.style.borderTop = "1px solid rgba(255,255,255,0.2)" : a.content.style.borderTop = "1px solid rgba(0,0,0,0.2)", t.border) {
        var _e24 = t.border,
            _n7 = _e24.lastIndexOf(" "),
            _o5 = _e24.slice(++_n7);

        jsPanel.colorNames[_o5] && (_e24 = _e24.replace(_o5, "#" + jsPanel.colorNames[_o5])), a.style.border = _e24;
      }

      return a;
    }, a.applyThemeBorder = function () {
      var t = jsPanel.pOborder(e.border);
      t[2].length ? jsPanel.colorNames[t[2]] && (t[2] = "#" + jsPanel.colorNames[t[2]]) : t[2] = a.style.backgroundColor, t = t.join(" "), a.style.border = t, a.options.border = t;
    }, a.applyThemeBorderRadius = function (e) {
      var t = "string" == typeof e ? e.trim().match(/\d*\.?\d+[a-zA-Z]{2,4}/i) : e + "px",
          n = a.content.style;
      return t ? (t = "string" == typeof t ? t : t[0], a.style.borderRadius = t, a.querySelector(".jsPanel-hdr") ? (a.header.style.borderTopLeftRadius = t, a.header.style.borderTopRightRadius = t) : (n.borderTopLeftRadius = t, n.borderTopRightRadius = t), a.querySelector(".jsPanel-ftr.active") ? (a.footer.style.borderBottomRightRadius = t, a.footer.style.borderBottomLeftRadius = t) : (n.borderBottomRightRadius = t, n.borderBottomLeftRadius = t), a.options.borderRadius = t) : a.options.borderRadius = void 0, a;
    }, a.setTheme = function () {
      var t = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : e.theme;
      var n = arguments.length > 1 ? arguments[1] : undefined;
      var o;
      if ("minimized" === a.status && (o = !0, a.normalize()), a.clearTheme(), "object" == _typeof(t)) e.border = void 0, a.applyCustomTheme(t);else {
        if ("none" === t) return a.style.backgroundColor = "#fff", a.header.classList.add("jsPanel-hdr-light"), o && a.minimize(), a;

        var _n8 = a.getThemeDetails(t);

        a.applyColorTheme(_n8), e.border ? a.applyThemeBorder() : (a.style.borderWidth = "", a.style.borderStyle = "", a.style.borderColor = "");
      }
      return o && a.minimize(), n && n.call(a, a), a;
    }, a.close = function (e) {
      jsPanel.close(a, e);
    }, a.maximize = function (t) {
      if (a.statusBefore = a.status, e.onbeforemaximize && e.onbeforemaximize.length > 0 && !jsPanel.processCallbacks(a, e.onbeforemaximize, "some", a.statusBefore)) return a;
      document.dispatchEvent(d);
      var n = a.parentElement,
          o = e.maximizedMargin;
      return n === document.body ? (a.style.width = document.documentElement.clientWidth - o[1] - o[3] + "px", a.style.height = document.documentElement.clientHeight - o[0] - o[2] + "px", a.style.left = o[3] + "px", a.style.top = o[0] + "px", e.position.fixed || (a.style.left = window.pageXOffset + o[3] + "px", a.style.top = window.pageYOffset + o[0] + "px")) : (a.style.width = n.clientWidth - o[1] - o[3] + "px", a.style.height = n.clientHeight - o[0] - o[2] + "px", a.style.left = o[3] + "px", a.style.top = o[0] + "px"), jsPanel.removeMinimizedReplacement(a), a.status = "maximized", a.setControls([".jsPanel-btn-maximize", ".jsPanel-btn-smallifyrev"]), jsPanel.front(a), document.dispatchEvent(p), document.dispatchEvent(l), e.onstatuschange && jsPanel.processCallbacks(a, e.onstatuschange, "every", a.statusBefore), t && t.call(a, a, a.statusBefore), e.onmaximized && jsPanel.processCallbacks(a, e.onmaximized, "every", a.statusBefore), a;
    }, a.minimize = function (t) {
      if ("minimized" === a.status) return a;
      if (a.statusBefore = a.status, e.onbeforeminimize && e.onbeforeminimize.length > 0 && !jsPanel.processCallbacks(a, e.onbeforeminimize, "some", a.statusBefore)) return a;

      if (document.dispatchEvent(h), !document.getElementById("jsPanel-replacement-container")) {
        var _e25 = document.createElement("div");

        _e25.id = "jsPanel-replacement-container", document.body.append(_e25);
      }

      if (a.style.left = "-9999px", a.status = "minimized", document.dispatchEvent(f), document.dispatchEvent(l), e.onstatuschange && jsPanel.processCallbacks(a, e.onstatuschange, "every", a.statusBefore), e.minimizeTo) {
        var _t19,
            _n9,
            _o6 = a.createMinimizedReplacement();

        if ("default" === e.minimizeTo) document.getElementById("jsPanel-replacement-container").append(_o6);else if ("parentpanel" === e.minimizeTo) {
          var _e26 = (_n9 = a.closest(".jsPanel-content").parentElement).querySelectorAll(".jsPanel-minimized-box");

          (_t19 = _e26[_e26.length - 1]).append(_o6);
        } else "parent" === e.minimizeTo ? ((_t19 = (_n9 = a.parentElement).querySelector(".jsPanel-minimized-container")) || ((_t19 = document.createElement("div")).className = "jsPanel-minimized-container", _n9.append(_t19)), _t19.append(_o6)) : document.querySelector(e.minimizeTo).append(_o6);
      }

      return t && t.call(a, a, a.statusBefore), e.onminimized && jsPanel.processCallbacks(a, e.onminimized, "every", a.statusBefore), a;
    }, a.normalize = function (t) {
      return "normalized" === a.status ? a : (a.statusBefore = a.status, e.onbeforenormalize && e.onbeforenormalize.length > 0 && !jsPanel.processCallbacks(a, e.onbeforenormalize, "some", a.statusBefore) ? a : (document.dispatchEvent(r), a.style.width = a.currentData.width, a.style.height = a.currentData.height, a.style.left = a.currentData.left, a.style.top = a.currentData.top, jsPanel.removeMinimizedReplacement(a), a.status = "normalized", a.setControls([".jsPanel-btn-normalize", ".jsPanel-btn-smallifyrev"]), jsPanel.front(a), document.dispatchEvent(c), document.dispatchEvent(l), e.onstatuschange && jsPanel.processCallbacks(a, e.onstatuschange, "every", a.statusBefore), t && t.call(a, a, a.statusBefore), e.onnormalized && jsPanel.processCallbacks(a, e.onnormalized, "every", a.statusBefore), a));
    }, a.smallify = function (t) {
      if ("smallified" === a.status || "smallifiedmax" === a.status) return a;
      if (a.statusBefore = a.status, e.onbeforesmallify && e.onbeforesmallify.length > 0 && !jsPanel.processCallbacks(a, e.onbeforesmallify, "some", a.statusBefore)) return a;
      document.dispatchEvent(m), "normalized" === a.status && a.saveCurrentDimensions(), a.style.overflow = "hidden";
      var n = window.getComputedStyle(a),
          o = parseFloat(window.getComputedStyle(a.headerbar).height);
      a.style.height = parseFloat(n.borderTopWidth) + parseFloat(n.borderBottomWidth) + o + "px", "normalized" === a.status ? (a.setControls([".jsPanel-btn-normalize", ".jsPanel-btn-smallify"]), a.status = "smallified", document.dispatchEvent(g), document.dispatchEvent(l), e.onstatuschange && jsPanel.processCallbacks(a, e.onstatuschange, "every", a.statusBefore)) : "maximized" === a.status && (a.setControls([".jsPanel-btn-maximize", ".jsPanel-btn-smallify"]), a.status = "smallifiedmax", document.dispatchEvent(u), document.dispatchEvent(l), e.onstatuschange && jsPanel.processCallbacks(a, e.onstatuschange, "every", a.statusBefore));
      var i = a.querySelectorAll(".jsPanel-minimized-box");
      return i[i.length - 1].style.display = "none", t && t.call(a, a, a.statusBefore), e.onsmallified && jsPanel.processCallbacks(a, e.onsmallified, "every", a.statusBefore), a;
    }, a.unsmallify = function (t) {
      if (a.statusBefore = a.status, "smallified" === a.status || "smallifiedmax" === a.status) {
        if (e.onbeforeunsmallify && e.onbeforeunsmallify.length > 0 && !jsPanel.processCallbacks(a, e.onbeforeunsmallify, "some", a.statusBefore)) return a;
        document.dispatchEvent(b), a.style.overflow = "visible", jsPanel.front(a), "smallified" === a.status ? (a.style.height = a.currentData.height, a.setControls([".jsPanel-btn-normalize", ".jsPanel-btn-smallifyrev"]), a.status = "normalized", document.dispatchEvent(c), document.dispatchEvent(l), e.onstatuschange && jsPanel.processCallbacks(a, e.onstatuschange, "every", a.statusBefore)) : "smallifiedmax" === a.status ? a.maximize() : "minimized" === a.status && a.normalize();

        var _n10 = a.querySelectorAll(".jsPanel-minimized-box");

        _n10[_n10.length - 1].style.display = "flex", t && t.call(a, a, a.statusBefore), e.onunsmallified && jsPanel.processCallbacks(a, e.onunsmallified, "every", a.statusBefore);
      }

      return a;
    }, a.front = function (t) {
      var n = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : !0;
      return jsPanel.front(a), document.dispatchEvent(y), t && t.call(a, a), e.onfronted && n && jsPanel.processCallbacks(a, e.onfronted, "every", a.status), a;
    }, a.closeChildpanels = function (e) {
      return a.getChildpanels().forEach(function (e) {
        return jsPanel.close(e);
      }), e && e.call(a, a), a;
    }, a.getChildpanels = function (e) {
      var t = a.content.querySelectorAll(".jsPanel");
      return e && t.forEach(function (t, n, o) {
        e.call(t, t, n, o);
      }), t;
    }, a.isChildpanel = function (e) {
      var t = a.closest(".jsPanel-content"),
          n = t ? t.parentElement : null;
      return e && e.call(a, a, n), !!t && n;
    }, a.contentRemove = function (e) {
      return jsPanel.emptyNode(a.content), e && e.call(a, a), a;
    }, a.createMinimizedReplacement = function () {
      var t = jsPanel.createMinimizedTemplate(),
          n = window.getComputedStyle(a.headertitle).color,
          o = window.getComputedStyle(a),
          i = e.iconfont,
          s = t.querySelector(".jsPanel-controlbar");
      return "auto-show-hide" !== a.options.header ? jsPanel.setStyle(t, {
        backgroundColor: o.backgroundColor,
        backgroundPositionX: o.backgroundPositionX,
        backgroundPositionY: o.backgroundPositionY,
        backgroundRepeat: o.backgroundRepeat,
        backgroundAttachment: o.backgroundAttachment,
        backgroundImage: o.backgroundImage,
        backgroundSize: o.backgroundSize,
        backgroundOrigin: o.backgroundOrigin,
        backgroundClip: o.backgroundClip
      }) : t.style.backgroundColor = window.getComputedStyle(a.header).backgroundColor, t.id = a.id + "-min", t.querySelector(".jsPanel-headerbar").replaceChild(a.headerlogo.cloneNode(!0), t.querySelector(".jsPanel-headerlogo")), t.querySelector(".jsPanel-titlebar").replaceChild(a.headertitle.cloneNode(!0), t.querySelector(".jsPanel-title")), t.querySelector(".jsPanel-titlebar").setAttribute("title", a.headertitle.textContent), t.querySelector(".jsPanel-title").style.color = n, s.style.color = n, ["jsPanel-hdr-dark", "jsPanel-hdr-light"].forEach(function (e) {
        a.header.classList.contains(e) && t.querySelector(".jsPanel-hdr").classList.add(e);
      }), a.setIconfont(i, t), "onpointerdown" in window && t.querySelectorAll(".jsPanel-btn").forEach(function (e) {
        e.addEventListener("pointerdown", function (e) {
          e.preventDefault();
        }, !0);
      }), "enabled" === a.dataset.btnnormalize ? jsPanel.pointerup.forEach(function (e) {
        t.querySelector(".jsPanel-btn-normalize").addEventListener(e, function () {
          a.normalize();
        });
      }) : s.querySelector(".jsPanel-btn-normalize").style.display = "none", "enabled" === a.dataset.btnmaximize ? jsPanel.pointerup.forEach(function (e) {
        t.querySelector(".jsPanel-btn-maximize").addEventListener(e, function () {
          a.maximize();
        });
      }) : s.querySelector(".jsPanel-btn-maximize").style.display = "none", "enabled" === a.dataset.btnclose ? jsPanel.pointerup.forEach(function (e) {
        t.querySelector(".jsPanel-btn-close").addEventListener(e, function () {
          jsPanel.close(a);
        });
      }) : s.querySelector(".jsPanel-btn-close").style.display = "none", t;
    }, a.dragit = function (t) {
      var n = Object.assign({}, jsPanel.defaults.dragit, e.dragit),
          o = a.querySelectorAll(n.handles);
      return "disable" === t ? o.forEach(function (e) {
        e.style.pointerEvents = "none";
      }) : o.forEach(function (e) {
        e.style.pointerEvents = "auto";
      }), a;
    }, a.resizeit = function (e) {
      var t = a.querySelectorAll(".jsPanel-resizeit-handle");
      return "disable" === e ? t.forEach(function (e) {
        e.style.pointerEvents = "none";
      }) : t.forEach(function (e) {
        e.style.pointerEvents = "auto";
      }), a;
    }, a.getScaleFactor = function () {
      var e = a.getBoundingClientRect();
      return {
        x: e.width / a.offsetWidth,
        y: e.height / a.offsetHeight
      };
    }, a.calcSizeFactors = function () {
      var t = window.getComputedStyle(a);
      if ("window" === e.container) a.hf = parseFloat(a.style.left) / (window.innerWidth - parseFloat(a.style.width)), a.vf = parseFloat(a.style.top) / (window.innerHeight - parseFloat(t.height));else if (a.parentElement) {
        var _e27 = a.parentElement.getBoundingClientRect();

        a.hf = parseFloat(a.style.left) / (_e27.width - parseFloat(a.style.width)), a.vf = parseFloat(a.style.top) / (_e27.height - parseFloat(t.height));
      }
    }, a.saveCurrentDimensions = function () {
      var e = window.getComputedStyle(a);
      a.currentData.width = e.width, "normalized" === a.status && (a.currentData.height = e.height);
    }, a.saveCurrentPosition = function () {
      var e = window.getComputedStyle(a);
      a.currentData.left = e.left, a.currentData.top = e.top;
    }, a.reposition = function () {
      var n,
          o = e.position,
          i = !0;

      for (var _len = arguments.length, t = new Array(_len), _key = 0; _key < _len; _key++) {
        t[_key] = arguments[_key];
      }

      return t.forEach(function (e) {
        "string" == typeof e || "object" == _typeof(e) ? o = e : "boolean" == typeof e ? i = e : "function" == typeof e && (n = e);
      }), jsPanel.position(a, o), i && a.saveCurrentPosition(), n && n.call(a, a), a;
    }, a.repositionOnSnap = function (t) {
      var n = "0",
          o = "0",
          i = jsPanel.pOcontainment(e.dragit.containment);
      e.dragit.snap.containment && ("left-top" === t ? (n = i[3], o = i[0]) : "right-top" === t ? (n = -i[1], o = i[0]) : "right-bottom" === t ? (n = -i[1], o = -i[2]) : "left-bottom" === t ? (n = i[3], o = -i[2]) : "center-top" === t ? (n = i[3] / 2 - i[1] / 2, o = i[0]) : "center-bottom" === t ? (n = i[3] / 2 - i[1] / 2, o = -i[2]) : "left-center" === t ? (n = i[3], o = i[0] / 2 - i[2] / 2) : "right-center" === t && (n = -i[1], o = i[0] / 2 - i[2] / 2)), jsPanel.position(a, t), jsPanel.setStyle(a, {
        left: "calc(".concat(a.style.left, " + ").concat(n, "px)"),
        top: "calc(".concat(a.style.top, " + ").concat(o, "px)")
      });
    }, a.overlaps = function (e, t, n) {
      return jsPanel.overlaps(a, e, t, n);
    }, a.shiftWithinParent = function (e) {
      var t = e.getBoundingClientRect(),
          n = a.options.maximizedMargin;
      var o, i;
      a.style.left = function () {
        return (o = (t.width - parseFloat(a.style.width)) * a.hf) <= n[3] ? "".concat(n[3], "px") : "".concat(o, "px");
      }(), a.style.top = function () {
        return (i = (t.height - parseFloat(a.currentData.height)) * a.vf) <= n[0] ? "".concat(n[0], "px") : "".concat(i, "px");
      }();
    }, a.setSize = function () {
      if (e.panelSize) {
        var _t20 = jsPanel.pOsize(a, e.panelSize);

        a.style.width = _t20.width, a.style.height = _t20.height;
      } else if (e.contentSize) {
        var _t21 = jsPanel.pOsize(a, e.contentSize);

        a.content.style.width = _t21.width, a.content.style.height = _t21.height, a.style.width = _t21.width, a.content.style.width = "100%";
      }

      return a;
    }, a.resize = function () {
      var t,
          n = window.getComputedStyle(a),
          o = {
        width: n.width,
        height: n.height
      },
          i = !0;

      for (var _len2 = arguments.length, e = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        e[_key2] = arguments[_key2];
      }

      e.forEach(function (e) {
        "string" == typeof e ? o = e : "object" == _typeof(e) ? o = Object.assign(o, e) : "boolean" == typeof e ? i = e : "function" == typeof e && (t = e);
      });
      var s = jsPanel.pOsize(a, o);
      return a.style.width = s.width, a.style.height = s.height, i && a.saveCurrentDimensions(), t && t.call(a, a), a;
    }, a.setControls = function (e, t) {
      return a.header.querySelectorAll(".jsPanel-btn").forEach(function (e) {
        var t = e.className.split("-"),
            n = t[t.length - 1];
        "hidden" !== a.getAttribute("data-btn".concat(n)) && (e.style.display = "block");
      }), e.forEach(function (e) {
        var t = a.controlbar.querySelector(e);
        t && (t.style.display = "none");
      }), t && t.call(a, a), a;
    }, a.setControlStatus = function (e) {
      var t = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "enable";
      var n = arguments.length > 2 ? arguments[2] : undefined;
      var o = a.controlbar.querySelector(".jsPanel-btn-".concat(e));
      return "disable" === t ? "removed" !== a.getAttribute("data-btn".concat(e)) && (a.setAttribute("data-btn".concat(e), "disabled"), o.style.pointerEvents = "none", o.style.opacity = .4, o.style.cursor = "default") : "hide" === t ? "removed" !== a.getAttribute("data-btn".concat(e)) && (a.setAttribute("data-btn".concat(e), "hidden"), o.style.display = "none") : "show" === t ? "removed" !== a.getAttribute("data-btn".concat(e)) && (a.setAttribute("data-btn".concat(e), "enabled"), o.style.display = "block", o.style.pointerEvents = "auto", o.style.opacity = 1, o.style.cursor = "pointer") : "enable" === t ? "removed" !== a.getAttribute("data-btn".concat(e)) && ("hidden" === a.getAttribute("data-btn".concat(e)) && (o.style.display = "block"), a.setAttribute("data-btn".concat(e), "enabled"), o.style.pointerEvents = "auto", o.style.opacity = 1, o.style.cursor = "pointer") : "remove" === t && (a.controlbar.removeChild(o), a.setAttribute("data-btn".concat(e), "removed")), n && n.call(a, a), a;
    }, a.setControlSize = function (e) {
      var t = e.toLowerCase();
      a.controlbar.querySelectorAll(".jsPanel-btn").forEach(function (e) {
        ["jsPanel-btn-xl", "jsPanel-btn-lg", "jsPanel-btn-md", "jsPanel-btn-sm", "jsPanel-btn-xs"].forEach(function (t) {
          e.classList.remove(t);
        }), e.classList.add("jsPanel-btn-".concat(t));
      }), "xl" === t ? a.titlebar.style.fontSize = "1.5rem" : "lg" === t ? a.titlebar.style.fontSize = "1.25rem" : "md" === t ? a.titlebar.style.fontSize = "1.05rem" : "sm" === t ? a.titlebar.style.fontSize = ".9rem" : "xs" === t && (a.titlebar.style.fontSize = ".8rem");
    }, a.setHeaderControls = function (t) {
      var n = jsPanel.pOheaderControls(e.headerControls);
      return ["close", "maximize", "normalize", "minimize", "smallify", "smallifyrev"].forEach(function (e) {
        n[e] && a.setControlStatus(e, n[e]);
      }), a.setControlSize(n.size), t && t.call(a, a), a;
    }, a.setHeaderLogo = function (e, t) {
      var n = [a.headerlogo],
          o = document.querySelector("#" + a.id + "-min");
      return o && n.push(o.querySelector(".jsPanel-headerlogo")), "string" == typeof e ? "<" !== e.substr(0, 1) ? n.forEach(function (t) {
        jsPanel.emptyNode(t);
        var n = document.createElement("img");
        n.src = e, t.append(n);
      }) : n.forEach(function (t) {
        t.innerHTML = e;
      }) : n.forEach(function (t) {
        jsPanel.emptyNode(t), t.append(e);
      }), n.forEach(function (e) {
        e.querySelectorAll("img").forEach(function (e) {
          e.style.maxHeight = getComputedStyle(a.headerbar).height;
        });
      }), t && t.call(a, a), a;
    }, a.setHeaderRemove = function (e) {
      return a.removeChild(a.header), a.content.classList.add("jsPanel-content-noheader"), ["close", "maximize", "normalize", "minimize", "smallify", "smallifyrev"].forEach(function (e) {
        a.setAttribute("data-btn".concat(e), "removed");
      }), e && e.call(a, a), a;
    }, a.setHeaderTitle = function (e, t) {
      var n = [a.headertitle],
          o = document.querySelector("#" + a.id + "-min");
      return o && n.push(o.querySelector(".jsPanel-title")), "string" == typeof e ? n.forEach(function (t) {
        t.innerHTML = e;
      }) : "function" == typeof e ? n.forEach(function (t) {
        jsPanel.emptyNode(t), t.innerHTML = e();
      }) : n.forEach(function (t) {
        jsPanel.emptyNode(t), t.append(e);
      }), t && t.call(a, a), a;
    }, a.setIconfont = function () {
      var e = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : !1;
      var t = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : a;
      var n = arguments.length > 2 ? arguments[2] : undefined;

      if (!1 !== e) {
        var _n11, _o7;

        if ("bootstrap" === e || "glyphicon" === e) _n11 = ["glyphicon glyphicon-remove", "glyphicon glyphicon-fullscreen", "glyphicon glyphicon-resize-full", "glyphicon glyphicon-minus", "glyphicon glyphicon-chevron-down", "glyphicon glyphicon-chevron-up"];else if ("fa" === e || "far" === e || "fal" === e || "fas" === e) _n11 = ["".concat(e, " fa-window-close"), "".concat(e, " fa-window-maximize"), "".concat(e, " fa-window-restore"), "".concat(e, " fa-window-minimize"), "".concat(e, " fa-chevron-down"), "".concat(e, " fa-chevron-up")];else if ("material-icons" === e) _n11 = [e, e, e, e, e, e], _o7 = ["close", "fullscreen", "fullscreen_exit", "call_received", "expand_more", "expand_less"];else {
          if (!Array.isArray(e)) return t;
          _n11 = ["custom-control-icon ".concat(e[5]), "custom-control-icon ".concat(e[4]), "custom-control-icon ".concat(e[3]), "custom-control-icon ".concat(e[2]), "custom-control-icon ".concat(e[1]), "custom-control-icon ".concat(e[0])];
        }
        t.querySelectorAll(".jsPanel-controlbar .jsPanel-btn").forEach(function (e) {
          jsPanel.emptyNode(e).innerHTML = "<span></span>";
        }), Array.prototype.slice.call(t.querySelectorAll(".jsPanel-controlbar .jsPanel-btn > span")).reverse().forEach(function (t, a) {
          t.className = _n11[a], "material-icons" === e && (t.textContent = _o7[a]);
        });
      }

      return n && n.call(t, t), t;
    }, a.addToolbar = function (e, t, n) {
      if ("header" === e ? e = a.headertoolbar : "footer" === e && (e = a.footer), "string" == typeof t) e.innerHTML = t;else if (Array.isArray(t)) t.forEach(function (t) {
        "string" == typeof t ? e.innerHTML += t : e.append(t);
      });else if ("function" == typeof t) {
        var _n12 = t.call(a, a);

        "string" == typeof _n12 ? e.innerHTML = _n12 : e.append(_n12);
      } else e.append(t);
      return e.classList.add("active"), n && n.call(a, a), a;
    }, a.setRtl = function () {
      [a.header, a.headerlogo, a.titlebar, a.headertitle, a.controlbar, a.headertoolbar, a.content, a.footer].forEach(function (t) {
        t.dir = "rtl", e.rtl.lang && (t.lang = e.rtl.lang);
      });
    }, a.id = e.id, a.classList.add("jsPanel-" + e.paneltype), "standard" === e.paneltype && (a.style.zIndex = this.zi.next()), o.append(a), a.front(!1, !1), a.setTheme(e.theme), e.boxShadow && a.classList.add("jsPanel-depth-".concat(e.boxShadow)), e.header) {
      if (e.headerLogo && a.setHeaderLogo(e.headerLogo), a.setIconfont(e.iconfont), a.setHeaderTitle(e.headerTitle), a.setHeaderControls(), "auto-show-hide" === e.header) {
        var _t22 = "jsPanel-depth-" + e.boxShadow;

        a.header.style.opacity = 0, a.style.backgroundColor = "transparent", this.remClass(a, _t22), this.setClass(a.content, _t22), a.header.addEventListener("mouseenter", function () {
          a.header.style.opacity = 1, jsPanel.setClass(a, _t22), jsPanel.remClass(a.content, _t22);
        }), a.header.addEventListener("mouseleave", function () {
          a.header.style.opacity = 0, jsPanel.remClass(a, _t22), jsPanel.setClass(a.content, _t22);
        });
      }
    } else a.setHeaderRemove();

    if (e.headerToolbar && a.addToolbar(a.headertoolbar, e.headerToolbar), e.footerToolbar && a.addToolbar(a.footer, e.footerToolbar), e.borderRadius && a.applyThemeBorderRadius(e.borderRadius), e.content && ("function" == typeof e.content ? e.content.call(a, a) : "string" == typeof e.content ? a.content.innerHTML = e.content : a.content.append(e.content)), e.contentAjax && this.ajax(a, e.contentAjax), e.contentFetch && this.fetch(a), e.contentOverflow) {
      var _t23 = e.contentOverflow.split(" ");

      1 === _t23.length ? a.content.style.overflow = _t23[0] : 2 === _t23.length && (a.content.style.overflowX = _t23[0], a.content.style.overflowY = _t23[1]);
    }

    if (e.rtl && a.setRtl(), a.setSize(), a.status = "normalized", e.position ? this.position(a, e.position) : a.style.opacity = 1, document.dispatchEvent(c), a.calcSizeFactors(), e.animateIn && (a.addEventListener("animationend", function () {
      _this.remClass(a, e.animateIn);
    }), this.setClass(a, e.animateIn)), e.syncMargins) {
      var _t24 = this.pOcontainment(e.maximizedMargin);

      e.dragit && (e.dragit.containment = _t24, e.dragit.snap && (e.dragit.snap.containment = !0)), e.resizeit && (e.resizeit.containment = _t24);
    }

    if (e.dragit ? (this.dragit(a, e.dragit), a.addEventListener("jspaneldragstop", function (e) {
      e.detail === a.id && a.calcSizeFactors();
    }, !1)) : a.titlebar.style.cursor = "default", e.resizeit) {
      var _t25;

      this.resizeit(a, e.resizeit), a.addEventListener("jspanelresizestart", function (e) {
        e.detail === a.id && (_t25 = a.status);
      }, !1), a.addEventListener("jspanelresizestop", function (n) {
        n.detail === a.id && ("smallified" === _t25 || "smallifiedmax" === _t25 || "maximized" === _t25) && parseFloat(a.style.height) > parseFloat(window.getComputedStyle(a.header).height) && (a.setControls([".jsPanel-btn-normalize", ".jsPanel-btn-smallifyrev"]), a.status = "normalized", document.dispatchEvent(c), document.dispatchEvent(l), e.onstatuschange && jsPanel.processCallbacks(a, e.onstatuschange, "every"), a.calcSizeFactors());
      }, !1);
    }

    if (a.saveCurrentDimensions(), a.saveCurrentPosition(), e.setStatus && ("smallifiedmax" === e.setStatus ? a.maximize().smallify() : "smallified" === e.setStatus ? a.smallify() : a[e.setStatus.substr(0, e.setStatus.length - 1)]()), e.autoclose && (a.closetimer = window.setTimeout(function () {
      a && jsPanel.close(a);
    }, e.autoclose)), this.pointerdown.forEach(function (t) {
      a.addEventListener(t, function (t) {
        t.target.closest(".jsPanel-btn-close") || t.target.closest(".jsPanel-btn-minimize") || "standard" !== e.paneltype || a.front();
      }, !1);
    }), e.onwindowresize && window.addEventListener("resize", function (t) {
      if (t.target === window) {
        var _n13,
            _o8 = e.onwindowresize,
            _i3 = a.status;

        if (!a.parentElement) return !1;
        _n13 = window.getComputedStyle(a.parentElement), "maximized" === _i3 && !0 === _o8 ? a.maximize() : a.snapped ? jsPanel.snapPanel(a, a.snapped, !0) : "normalized" !== _i3 && "smallified" !== _i3 && "maximized" !== _i3 || ("function" == typeof _o8 ? _o8.call(a, t, a) : (a.style.left = function () {
          var t;
          return (t = "window" === e.container ? (window.innerWidth - parseFloat(a.style.width)) * a.hf : (parseFloat(_n13.width) - parseFloat(a.style.width)) * a.hf) <= 0 ? 0 : t + "px";
        }(), a.style.top = function () {
          var t;
          return (t = "window" === e.container ? (window.innerHeight - parseFloat(a.currentData.height)) * a.vf : (parseFloat(_n13.height) - parseFloat(a.currentData.height)) * a.vf) <= 0 ? 0 : t + "px";
        }()));
      }
    }, !1), e.onparentresize) {
      var _t26 = a.isChildpanel(),
          _n14 = _t26.options,
          _o9 = "function" == typeof e.onparentresize,
          _i4 = function _i4() {
        "minimized" !== a.status && ("maximized" === a.status ? a.maximize() : "smallifiedmax" === a.status ? a.maximize().smallify() : a.snapped ? jsPanel.snapPanel(a, a.snapped, !0) : a.shiftWithinParent(_t26.content));
      };

      _t26 && (_o9 ? _n14.resizeit.resize.push(function () {
        if ("minimized" !== a.status) {
          var _n15 = _t26.content.getBoundingClientRect();

          e.onparentresize.call(a, a, {
            width: _n15.width,
            height: _n15.height
          });
        }
      }) : (_n14.resizeit.resize.push(function () {
        _i4();
      }), _n14.onmaximized.push(function () {
        _i4();
      }), _n14.onnormalized.push(function () {
        _i4();
      }), a.options.onnormalized.push(function () {
        a.snapped ? jsPanel.snapPanel(a, a.snapped, !0) : a.shiftWithinParent(_t26.content);
      })));
    }

    return this.globalCallbacks && (Array.isArray(this.globalCallbacks) ? this.globalCallbacks.forEach(function (e) {
      e.call(a, a);
    }) : this.globalCallbacks.call(a, a)), e.callback && (Array.isArray(e.callback) ? e.callback.forEach(function (e) {
      e.call(a, a);
    }) : e.callback.call(a, a)), t && t.call(a, a), document.dispatchEvent(i), a;
  }
};
exports.jsPanel = jsPanel;
"undefined" != typeof module && (module.exports = jsPanel);

},{}]},{},[1]);
