var $ = require('jquery')
var MetaGrid = require('./metagrid.js')
var jsPanel = require('./jspanel/jspanel.js')

//use this to track whether we should override panel positions
// on window size
var userHasNotModifiedLayout = true;

/* TODO INCLUDE THESE AS CONFIG OPTIONS NOT GLOBALS ... */

//extra height added to content area to prevent scrollbar appearing
var jspanelExtraHeight = 6; 
var jspanelExtraWidth = 6;

//amount to subtract from panel size to calculate content height after resize
var jspanelHeaderHeight = 29; 


var configuration = { grids:[
		{
			'id':'small',
			'cols':3,
			'rows':11,
			'minwidth':0,
			'sizeRule':'fullWidth'
		},
		{   'id':'big',
			'cols':16,
			'rows':9,
			'minwidth':680,
			'sizeRule':'showAll'
		}
	]
}

var grid = new MetaGrid('bigsmall');

grid.createGrid(configuration);

console.log(grid)

var useMasterPanel = false;

if (useMasterPanel){
 grid.addPanel('masterPanel',  0, 0, 0, 0, 'small');
 grid.addPanel('masterPanel',  0, 0, 0, 0, 'big');
}


grid.addPanel('renderPanel',  0, 0, 8, 7, 'big');
grid.addPanel('scrollPanel',  0,   0, 0, 0, 'big');

grid.addPanel('scrollPanel',  0,   0, 3, 0.20, 'small', true);
grid.addPanel('renderPanel',  0,   0.20 , 3,   3, 'small');





/******************************************************************************

Create jsPanels 
 
/*****************************************************************************/
$( document ).ready(function() { 

  grid.setPageSize($(window).innerWidth(), $(window).innerHeight());
  grid.setActiveGrid();

  if (useMasterPanel){
    var id = 'masterPanel'
    var dims = grid.getPanelDims(id) 

    var masterPanel = jsPanel.create({
      id: id,
      border:"0px",
      header: false,

      maximizedMargin: 0,
      syncMargins: true,
      theme:       'primary',
      contentOverflow: 'auto',
      contentSize: {
        width: dims.w,
        height: dims.h
      },
      position:    dims.p, 
      animateIn:   'jsPanelFadeIn',
      onwindowresize: false,
      content:     '<div id="masterPanelContent"></div>',
    });
    var panelContainer = masterPanel.content;
    $("#master").detach().appendTo('#masterPanelContent');
  }

  else{
    var panelContainer = document.body
  }

	var id = 'scrollPanel'
	var dims = grid.getPanelDims(id) 

	var renderPanel =	jsPanel.create({
                container:   panelContainer,
		id: id,
		maximizedMargin: 0,
		syncMargins: true,
		theme:       'primary',
		contentSize: {
			width: dims.w - jspanelExtraWidth,
			height: dims.h
		},
		position:    dims.p, 
                header: false, 
		headerControls: "none",
		animateIn:   'jsPanelFadeIn',
		content:     '<div id="scrollPanelContent"></div>',
		onwindowresize: false
	});

	var id = 'renderPanel'
	var dims = grid.getPanelDims(id) 

	var renderPanel =	jsPanel.create({
                container:   panelContainer,
		id: id,
		maximizedMargin: 0,
		syncMargins: true,
		theme:       'primary',
		contentSize: {
			width: dims.w - jspanelExtraWidth,//function() { return renderInitialWidth},
			height: dims.h - jspanelHeaderHeight//function() { return renderInitialHeight + jspanelExtraHeight}
		},
		position:    dims.p, //'left-top 0 0',
		headerControls: "none",
		animateIn:   'jsPanelFadeIn',
		headerTitle: 'APP #1', //TODO get from lat,lon vars
		content:     '<div id="renderPanelContent"></div>',
		onwindowresize: false
	});
  

	
	$("#renderHere").detach().appendTo('#renderPanelContent')
    $("#scroll").detach().appendTo('#scrollPanelContent')




  document.addEventListener('jspanelresize', function (event) {
    console.log(event)
    //get dimensions of the resized panel (0th panel in array)
    var panelHeight = parseFloat(jsPanel.getPanels()[0].style.height).toFixed(0);
    var panelWidth = parseFloat(jsPanel.getPanels()[0].style.width).toFixed(0); 
    
    
    //adjust for the usable area
    var contentHeight = panelHeight - jspanelHeaderHeight;
    var contentWidth  = panelWidth - jspanelExtraWidth;
    console.log(event.detail, contentWidth, contentHeight)
    if (event.detail === 'renderPanel') {resizeRender(contentHeight, contentWidth);}
    if (event.detail === 'blocklyPanel') {resizeBlockly(contentHeight, contentWidth);}
    if (event.detail === 'graphPanel') {resizeGraph(contentHeight, contentWidth);}
    if (event.detail === 'sensorPanel') {resizeSensor(contentHeight, contentWidth);}
    if (event.detail === 'scorePanel') {resizeScore(contentHeight, contentWidth);}
  });

 
  //setInterval(function(){ console.log(new Date());}, 300); 
  setTimeout(function(){ layoutAllPanels(); }, 100);

}); //ready

document.addEventListener('jspanelresizestop', function (event) {
	userHasNotModifiedLayout = false;
});

document.addEventListener('jspaneldragstop', function (event) {
	userHasNotModifiedLayout = false;
});	

$( window ).resize(function() {
  console.log("window resize")
  if (true){   
    //console.log('laying out jspanels')
	grid.setPageSize($(window).innerWidth(), $(window).innerHeight());
        grid.setActiveGrid();
	layoutAllPanels();
  }
 
});

function scrollPanels(){
	grid.setPageSize($(window).innerWidth(), $(window).innerHeight());
        grid.setActiveGrid();
	layoutAllPanels();
        scrollPanel.front(); //keep scroll panel on top
}



function layoutAllPanels(){

 /* log({"category":"display",
       "data":{
         "event":"windowResize",
         "window":{
           "width":$(window).innerWidth(),
           "height":$(window).innerHeight()
         }
       }
      });
*/
  for (const panel of jsPanel.getPanels()) {

    var dims = grid.getPanelDims(panel.id)
    panel.resize(dims.size)
    
    panel.reposition(dims.p)
    /*
    log({"category":"display",
         "data":{
           "event":"panelLayout",
           "panel":{
             "id":panel.id,
             "size":dims.size,
             "position":dims.p
           }
         }
        });
    */
   
    var panelHeight = parseFloat(panel.style.height).toFixed(0);
    var panelWidth = parseFloat(panel.style.width).toFixed(0);
    var contentHeight = panelHeight - jspanelHeaderHeight;
    var contentWidth  = panelWidth;			
    if (panel.id === 'renderPanel') {resizeRender(contentHeight, contentWidth);}
    if (panel.id === 'blocklyPanel') {resizeBlockly(contentHeight, contentWidth);}
    if (panel.id === 'graphPanel') {resizeGraph(contentHeight, contentWidth);}
    if (panel.id === 'sensorPanel') {resizeSensor(contentHeight, contentWidth);}
    if (panel.id === 'scorePanel') {resizeScore(contentHeight, contentWidth);}
    if (panel.id === 'masterPanel') {resizeMaster(contentHeight, contentWidth);}
    
  }


}



function resizeCanvasById(id,height, width){
	var canvas = document.getElementById(id)
	canvas.width = width;
	canvas.height = height;	
	
}

function resizeScore(height, width){
	//if (scoreDial !== undefined) scoreDial.setSize(height, width);
	
}

function resizeMaster(height, width){
	resizeCanvasById('master', height, width);
}

function resizeSensor(height, width){
	resizeCanvasById('sensors', height, width);
}

function resizeGraph(height, width){
	var graphArea = document.getElementById('graphHere')
	graphArea.width = width;
	graphArea.height = height;
}

function resizeBlockly(height, width){
	var blocklyArea = document.getElementById('blocklyArea')
	blocklyArea.offsetHeight = height;
	blocklyArea.offsetWidth = width; 
	//blockly_onresize();
}

function resizeRender(height, width){
	//renderer.setSize( width, height);
	//camera.aspect = width / height;
	//camera.updateProjectionMatrix();		
}
	
